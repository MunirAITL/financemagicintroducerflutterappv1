package uk.co.financemagic.introducer
import io.flutter.app.FlutterApplication
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback
import io.flutter.plugins.GeneratedPluginRegistrant


/*
class Application : FlutterApplication(), PluginRegistrantCallback {

    private val CHANNEL = "uk.co.financemagic.introducer/twillio"

    override fun onCreate() {
        super.onCreate()
        FlutterFirebaseMessagingService.setPluginRegistrant(this)
        */
/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("messages", "Messages", NotificationManager.IMPORTANCE_HIGH)
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel)
        }*//*

    }

    override fun registerWith(registry: PluginRegistry) {
        FirebaseMessagingPlugin.registerWith(registry.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin"))
    }
}*/


// ...


class Application : FlutterApplication(), PluginRegistrantCallback {
    // ...
    override fun onCreate() {
        super.onCreate()
        //FlutterFirebaseMessagingBackgroundService.setPluginRegistrant(this)
    }

    override fun registerWith(registry: PluginRegistry?) {
       try{
           GeneratedPluginRegistrant.registerWith(FlutterEngine(applicationContext))
       }catch (e:Exception){

       }
    }
}