package uk.co.financemagic.introducer

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.legacy.content.WakefulBroadcastReceiver
import com.google.firebase.messaging.RemoteMessage
import java.lang.Exception

class FirebaseBroadcastReceiver : BroadcastReceiver() {

    val TAG: String = FirebaseBroadcastReceiver::class.java.simpleName


    override fun onReceive(context: Context, intent: Intent) {

        val dataBundle = intent.extras
        if (dataBundle != null) {
            for (key in dataBundle.keySet()) {
                Log.d(TAG, "FirebaseBroadcastReceiver->dataBundle: " + key + " : " + dataBundle.get(key))
            }
            try {
                println("Receiver start")
                val local = Intent()
                local.action = "service.to.activity.incoming"
                local.putExtra("message", dataBundle?.get("body").toString())
                context.sendBroadcast(local)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}