import 'dart:convert';
import 'dart:developer' as dev;
import 'dart:io';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/view_model/rx/UploadProgController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';
import 'package:synchronized/synchronized.dart';

class APIViewModel<T> {
  Future<void> req<T>({
    @required BuildContext context,
    @required String url,
    @required ReqType reqType,
    Map<String, dynamic> param,
    headers,
    bool isLoading = true,
    bool isCookie = false,
    Function(T) callback,
  }) async {
    try {
      final progController = Get.put(UploadProgController());
      var lock = new Lock();
      await lock.synchronized(() async {
        final jsonString = JsonString(json.encode(param));
        dev.log(jsonString.source);
        await NetworkMgr.shared
            .req<T, Null>(
          context: context,
          url: url,
          param: param,
          reqType: reqType,
          headers: headers,
          isLoading: isLoading,
          isCookie: isCookie,
          progController: progController,
        )
            .then((model) async {
          lock = null;
          if (callback != null) callback(model);
        });
      });
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> upload({
    context,
    File file,
    Function(MediaUploadFilesAPIModel) callback,
  }) async {
    try {
      final progController = Get.put(UploadProgController());
      var lock = new Lock();
      await lock.synchronized(() async {
        await Future.wait([
          NetworkMgr.shared
              .uploadFiles<MediaUploadFilesAPIModel, Null>(
            context: context,
            url: "", //APIMediaCfg.SELFIE_URL,
            files: [file],
            progController: progController,
          )
              .then((model) async {
            lock = null;
            if (callback != null) callback(model);
          })
        ]);
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
