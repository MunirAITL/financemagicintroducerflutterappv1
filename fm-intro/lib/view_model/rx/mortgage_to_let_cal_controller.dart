import 'package:get/get.dart';

class MortgageToLetCalController extends GetxController {
  //  1
  var propertyValue = 300000.0.obs;
  var rentalIncome = 1500.0.obs;
  //  2
  var depositValue = 100000.0.obs;
  var interestRate = 2.5.obs;
  //
  var maxLoanAmount = 0.0.obs;
  var stampDutyAmount = 0.0.obs;
  var lTVAmount = 66.0.obs;
  var monthlyInterestAmount = 410.0.obs;
}
