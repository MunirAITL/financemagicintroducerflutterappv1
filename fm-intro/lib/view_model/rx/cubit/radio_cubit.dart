import 'package:flutter_bloc/flutter_bloc.dart';

class RadioCubit extends Cubit<bool> {
  RadioCubit() : super(false);
  void onChanged(bool v) => emit(!v);
}
