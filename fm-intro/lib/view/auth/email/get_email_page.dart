import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/auth/email/check_email_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../base_app.dart';

class GetEmailPage extends StatefulWidget {
  final email;
  const GetEmailPage({Key key, @required this.email}) : super(key: key);
  @override
  State createState() => _GetEmailPageState();
}

class _GetEmailPageState extends BaseApp<GetEmailPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //extendBodyBehindAppBar: true,
        backgroundColor: MyTheme.bgColor,
        appBar: drawAppbar(
          onBack: () {
            Get.back();
          },
          backTitle: "Back",
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Column(
                children: [
                  Container(
                      child: Image.asset("assets/images/img/email_image.png")),
                  SizedBox(height: 20),
                  Txt(
                    txt: "I didn't get my email",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    fontWeight: FontWeight.w500,
                    isBold: true,
                  ),
                  SizedBox(height: 20),
                  Txt(
                    txt: "We've sent an email to",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  Txt(
                    txt: widget.email,
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                    txt:
                        "Check your address is right - mistakes happen If it isn't, go back a screen and try again.",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                  Txt(
                    txt: "If it is correct:",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                  SizedBox(height: 15),
                  Txt(
                    txt: "1. Wait 10 minutes. Sometimes emails can be delayed.",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                  SizedBox(height: 5),
                  Txt(
                    txt:
                        "2. Check your Spam folder (and mark it as 'Not Spam' if it's in there).",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                  SizedBox(height: 5),
                  Txt(
                    txt: "3. Re-send the email with the button below.",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                  SizedBox(height: 5),
                  Txt(
                    txt:
                        "4. If none of that works, email help@monzo.com from the same email you just registered with us, and we'll help you out.",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: MMBtn(
                txt: "Next",
                txtColor: Colors.white,
                height: getHP(context, 7),
                width: getW(context),
                radius: 10,
                callback: () {
                  Get.back();
                },
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
