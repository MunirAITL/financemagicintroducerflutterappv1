import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/controller/api/db_intr/case_lead/DBIntrDashboardAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_intr/caselead/LeadStatusAdviserWiseReportDatasModel.dart';
import 'package:aitl/model/json/db_intr/caselead/UserPaymentSummaryDataModel.dart';
import 'package:aitl/model/json/db_intr/dashboard/GetChartAPIModel.dart';
import 'package:aitl/view/common/ActionReqPage.dart';
import 'package:aitl/view/db_intr/caselead/charts/SimpleBarChart.dart';
import 'package:aitl/view/db_intr/caselead/charts/SimpleLineChart.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:new_version/new_version.dart';
import '../../../model/json/db_intr/createlead/usernote/PostCaseNoteAPIModel.dart';
import '../../../view_model/helper/ui_helper.dart';
import '../../dashboard_main_base.dart';
import '../../widgets/progress/AppbarBotProgbar.dart';
import '../../widgets/txt/Txt.dart';
import '../createlead/leaddetails/create_lead.dart';
import '../customer/CreateLeadCase.dart';

class DashboardLeadTab extends StatefulWidget {
  @override
  State createState() => DashboardLeadTabState();
}

class DashboardLeadTabState extends BaseMainDashboard<DashboardLeadTab>
    with Mixin, StateListener {
  int completedVal = 25;
  int fmaVal = 22;
  int offeredVal = 5;
  int leadVal = 115;

  List<dynamic> listGrid = [];

  bool isShowSlider = true;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit_dashboard;

  StateProvider _stateProvider;
  static const String _iconPath = "assets/images/screens/db_intr";

  String refererCode = "335454";

  UserPaymentSummaryDataModel userPaymentSummaryDataModel;
  List<LeadStatusAdviserWiseReportDatasModel>
      listLeadStatusAdviserWiseReportDatasModel = [];
  List<CaseCountMonthlyWiseSummaryReports> caseCountMonthlyWiseSummaryReports =
      [];

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar1_reload_case_api) {
      await refreshData();
      checkNewerVersion();
    }
  }

  checkNewerVersion() async {
    // Instantiate NewVersion manager object (Using GCP Console app as example)
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) {
        final newVersion = NewVersion(
          iOSId: 'uk.co.financemagic.introducer',
          androidId: 'uk.co.financemagic.introducer',
        );
        newVersion.showAlertIfNecessary(context: context);
      }
    });
  }

  //  Action Required Start...

  Future<void> refreshData() async {
    try {
      if (mounted) {
        await onDashboardAPI();
      }
    } catch (e) {}
  }

  onDashboardAPI() async {
    if (mounted) {
      //  Lead Summary
      await DBIntrDashboardAPIMgr()
          .wsGetURLGetlLeadStatusIntrwiseReportDatabyIntr(
        context: context,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  listLeadStatusAdviserWiseReportDatasModel =
                      model.responseData.leadStatusAdviserWiseReportDatas;
                  if (listLeadStatusAdviserWiseReportDatasModel.length > 0) {
                    setLeadSumData();
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              } else {}
            } catch (e) {
              myLog(e.toString());
            }
          }
        },
      );
    }

    if (mounted) {
      //  payment summary
      await DBIntrDashboardAPIMgr().wsGetCasePayInfo(
          context: context,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    userPaymentSummaryDataModel =
                        model.responseData.userPaymentSummaryData;
                  } catch (e) {
                    myLog(e.toString());
                  }
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          });
    }

    if (mounted) {
      final date1 = DateFormat('dd-MMM-yyyy')
          .format(DateTime.now().subtract(new Duration(days: 7)));
      final date2 = DateFormat('dd-MMM-yyyy').format(DateTime.now());
      var url = ServerIntr.GET_CASE_COUNT_REPORT_INTR_BY_INTR_URL;
      url = url.replaceAll(
          "#userCompanyId#", userData.userModel.userCompanyID.toString());
      url = url.replaceAll("#fromDT#", date1);
      url = url.replaceAll("#toDT#", date2);
      url = url.replaceAll("#introId#", userData.userModel.id.toString());
      await APIViewModel().req<GetChartAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                caseCountMonthlyWiseSummaryReports =
                    model.responseData.caseCountMonthlyWiseSummaryReports;
              }
            }
          });
    }

    if (mounted) {
      setState(() {});
    }
  }

  //  Action Required End...

  setLeadSumData() {
    try {
      LeadStatusAdviserWiseReportDatasModel leadModel =
          listLeadStatusAdviserWiseReportDatasModel[0];
      listGrid.add({
        'title': 'Total Lead',
        'value': leadModel.totalLeads,
        'icon': _iconPath + "/grid_leadsum/total_leads.png",
      });
      listGrid.add({
        'title': 'Total Cases',
        'value': leadModel.totalCase,
        'icon': _iconPath + "/grid_leadsum/total_cases.png",
      });
      listGrid.add({
        'title': 'Completed Cases',
        'value': leadModel.totalCompletedCase,
        'icon': _iconPath + "/grid_leadsum/complete_cases.png",
      });
      listGrid.add({
        'title': 'FMA Submitted Cases',
        'value': 0,
        'icon': _iconPath + "/grid_leadsum/fma_submitted_cases.png",
      });
      listGrid.add({
        'title': 'Total Offered Cases',
        'value': 0,
        'icon': _iconPath + "/grid_leadsum/total_offered_cases.png",
      });
      listGrid.add({
        'title': 'Total Customers',
        'value': leadModel.totalConvertedLeads,
        'icon': _iconPath + "/grid_leadsum/total_customers.png",
      });

      listGrid.add({
        'title': 'Junk Leads',
        'value': leadModel.totalJunkLeads,
        'icon': _iconPath + "/grid_leadsum/total_cases.png",
      });
      /*listGrid.add({
        'title': 'Total Introducers',
        'value': 0,
        'icon': _iconPath+"grid/total_introducers.png",
      });*/
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    userPaymentSummaryDataModel = null;
    listLeadStatusAdviserWiseReportDatasModel = null;
    caseCountMonthlyWiseSummaryReports = null;
    listGrid = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      await refreshData();
      checkNewerVersion();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          elevation: MyTheme.appbarElevation,
          /*leading: IconButton(
              iconSize: 30,
              icon: Icon(Icons.add, color: Colors.white, size: 30),
              onPressed: () {
                Get.to(() => CreateCaseLeadScreen());
              }),*/
          title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                UIHelper().drawAppbarTitle(title: "Dashboard"),
                Spacer(),
                /*GestureDetector(
                  onTap: () {
                    Get.to(() => CreateLeadPage()).then((value) {
                      if (value != null) Get.back();
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 3,
                          horizontal: 3), //adds padding inside the button
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white)),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.add, size: 15),
                          Flexible(
                              child: Text(
                            "Add Lead",
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          )),
                          SizedBox(width: 5)
                        ],
                      ),
                    ),
                  ),
                )*/
              ]),
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.titleColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return isLoading
        ? SizedBox()
        : Container(
            child: SingleChildScrollView(
            primary: true,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MMBtn(
                          txt: "Action Required",
                          width: getW(context),
                          height: getHP(context, 7),
                          callback: () {
                            Get.to(() => ActionReqPage());
                          }),
                      SizedBox(height: 10),
                      MMBtn(
                          txt: "Introduce new case",
                          width: getW(context),
                          height: getHP(context, 7),
                          callback: () {
                            Get.to(() => CreateLeadCase(isNewCase: true));
                          }),
                      SizedBox(height: 10),
                      MMBtn(
                          txt: "Forward Lead",
                          width: getW(context),
                          height: getHP(context, 7),
                          callback: () async {
                            //Get.to(() => CreateLeadCase(isNewCase: true));
                            //  https://pub.dev/packages/flutter_share_me/example
                            String url =
                                Server.BASE_URL + '/referral/' + refererCode;
                            String msg =
                                'Hey managing your case would be so much easier if you had Finance magic. Sign up with my link ' +
                                    url;
                            final FlutterShareMe flutterShareMe =
                                FlutterShareMe();
                            String response =
                                await flutterShareMe.shareToSystem(msg: msg);
                            print(response);
                          }),
                      SizedBox(height: 10),
                      MMBtn(
                          txt: "Create new lead",
                          width: getW(context),
                          height: getHP(context, 7),
                          callback: () {
                            Get.to(() => CreateLeadPage());
                          }),
                    ],
                  ),
                ),
                //drawCircleLayout(),
                drawLeadSummary(),
                drawPaySummary(),
                drawBarChart(),
                drawLineChart(),
              ],
            ),
          ));
  }

  drawCircleLayout() {
    return Container(
      child: Card(
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          /*Padding(
            padding: const EdgeInsets.only(top: 20, left: 40, bottom: 40),
            child: Container(
              height: getWP(context, 40),
              width: getWP(context, 40),
              child: CustomPaint(
                foregroundPainter: new CirclePainter(
                  totalCases: 566,
                  completePA: 40,
                  fmaPA: 10,
                  offeredPA: 20,
                  width: getWP(context, 100),
                  height: getHP(context, 100),
                  fontSize:
                      getTxtSize(context: context, txtSize: MyTheme.txtSize),
                ),
              ),
            ),
          ),
          SizedBox(width: 50),
          Expanded(
            child: Container(
              //color: Colors.blue,
              //height: 100,
              //width: 200,
              child: Column(
                children: [
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.completedColor, completedVal, "Completed"),
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.fmaColor, fmaVal, "FMA Submitted"),
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.offeredColor, offeredVal, "Offered"),
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.leadColor, leadVal, "Total Lead"),
                ],
              ),
            ),
          ),*/
        ]),
      ),
    );
  }

  drawCircleRightIcons(Color colr, int val, String status) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(Icons.circle, color: colr, size: 10),
        SizedBox(width: 20),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: val.toString(),
                  txtColor: MyTheme.timelineTitleColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(height: 5),
              Txt(
                  txt: status,
                  txtColor: MyTheme.timelineTitleColor,
                  txtSize: MyTheme.txtSize - .7,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ],
          ),
        ),
      ],
    );
  }

  drawLeadSummary() {
    final double itemHeight = (getH(context) - kToolbarHeight - 24) / 2;
    final double itemWidth = getW(context) / 2;
    return (listGrid.length > 0)
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Txt(
                    txt: "Lead Summary",
                    txtColor: MyTheme.timelineTitleColor,
                    txtSize: MyTheme.txtSize + .4,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              GridView.count(
                shrinkWrap: true,
                primary: false,
                crossAxisCount: 2,
                padding: const EdgeInsets.all(0),
                scrollDirection: Axis.vertical,
                childAspectRatio: (itemHeight / itemWidth),
                children: List.generate(
                  listGrid.length,
                  (index) {
                    return Card(
                      margin: new EdgeInsets.all(10),
                      //color: Colors.black,
                      child: new Row(
                        //mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //footer: new Text(listGrid[index]['title']),
                          Expanded(
                            //flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Txt(
                                    txt: listGrid[index]['title'],
                                    txtColor: MyTheme.timelineTitleColor,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.center,
                                    isBold: true),
                                SizedBox(height: 10),
                                Flexible(
                                  child: Txt(
                                      txt: listGrid[index]['value'].toString(),
                                      txtColor: MyTheme.timelineTitleColor,
                                      txtSize: MyTheme.txtSize + 1,
                                      txtAlign: TextAlign.center,
                                      isBold: true),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: Container(
                              width: getWP(context, 14),
                              height: getWP(context, 14),
                              //color: Colors.black,
                              child: Image.asset(
                                listGrid[index]['icon'],
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          )
        : SizedBox();
  }

  drawPaySummary() {
    return (userPaymentSummaryDataModel != null)
        ? Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 10),
                  child: Txt(
                      txt: "Payment Summary",
                      txtColor: MyTheme.timelineTitleColor,
                      txtSize: MyTheme.txtSize + .4,
                      txtAlign: TextAlign.start,
                      isBold: true),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 5, right: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _drawPaySumCard(
                            "Total Income Received!",
                            userPaymentSummaryDataModel.totalIncomeAmount,
                            _iconPath + "/grid_paysum/total_income_rec.png",
                          ),
                          _drawPaySumCard(
                            "Paid Income",
                            userPaymentSummaryDataModel.totalPaidAmount,
                            _iconPath + "/grid_paysum/paid_income.png",
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _drawPaySumCard(
                            "Pipeline Income",
                            userPaymentSummaryDataModel.totalPipelineAmount,
                            _iconPath + "/grid_paysum/pipeline_income.png",
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        : SizedBox();
  }

  _drawPaySumCard(String title, double amounts, String icon) {
    return Container(
      width: getWP(context, 50) - 10,
      height: getWP(context, 35),
      child: Card(
        margin: new EdgeInsets.all(5),
        //color: Colors.black,
        child: new Row(
          //mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //footer: new Text(listGrid[index]['title']),
            Expanded(
              //flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Txt(
                      txt: title,
                      txtColor: MyTheme.timelineTitleColor,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.center,
                      maxLines: 3,
                      isBold: true),
                  SizedBox(height: 10),
                  Txt(
                      txt: AppDefine.CUR_SIGN + amounts.toStringAsFixed(0),
                      txtColor: MyTheme.timelineTitleColor,
                      txtSize: MyTheme.txtSize + .4,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ],
              ),
            ),
            SizedBox(width: 10),
            Flexible(
              child: Container(
                width: getWP(context, 14),
                height: getWP(context, 14),
                //color: Colors.black,
                child: Image.asset(
                  icon,
                  fit: BoxFit.cover,
                ),
              ),
            ),

            //just for testing, will fill with image later
          ],
        ),
      ),
    );
  }

  drawBarChart() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 10),
            child: Txt(
                txt: "Case Summary - Total Cases",
                txtColor: MyTheme.timelineTitleColor,
                txtSize: MyTheme.txtSize + .4,
                txtAlign: TextAlign.start,
                isOverflow: true,
                isBold: true),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 30),
            child: Row(
              children: [
                Row(
                  children: [
                    drawCircle(
                      context: context,
                      color: HexColor.fromHex("#C1272D"),
                      size: 3,
                    ),
                    SizedBox(width: 10),
                    Txt(
                        txt: "Total Cases",
                        txtColor: MyTheme.leadSubTitle,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ],
                ),
                SizedBox(width: 20),
                Expanded(
                  child: Row(
                    children: [
                      drawCircle(
                        context: context,
                        color: HexColor.fromHex("#CDCDCD"),
                        size: 3,
                      ),
                      SizedBox(width: 10),
                      Txt(
                          txt: "Completed Cases",
                          txtColor: MyTheme.leadSubTitle,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ],
                  ),
                ),
              ],
            ),
          ),
          caseCountMonthlyWiseSummaryReports.length > 0
              ? Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                      width: getWP(context, 100),
                      height: getHP(context, 40),
                      child: SimpleBarChart.withSampleData(
                          caseCountMonthlyWiseSummaryReports)),
                )
              : SizedBox(),
        ],
      ),
    );
  }

  drawLineChart() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 10),
            child: Txt(
                txt: "Income- Last 30 Days",
                txtColor: MyTheme.timelineTitleColor,
                txtSize: MyTheme.txtSize + .4,
                txtAlign: TextAlign.start,
                isOverflow: true,
                isBold: true),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Txt(
                txt: AppDefine.CUR_SIGN + "500",
                txtColor: MyTheme.leadSubTitle,
                txtSize: MyTheme.txtSize + 1,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
                width: getWP(context, 100),
                height: getHP(context, 40),
                child: SimpleLineChart.withSampleData()),
          ),
        ],
      ),
    );
  }
}
