//Here is my CustomLegendBuilder
import 'dart:math';
import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
//import 'package:charts_common/src/chart/common/behavior/legend/legend.dart';
//import 'package:flutter/material.dart';
import 'package:charts_flutter/src/text_element.dart' as ChartText;
import 'package:charts_flutter/src/text_style.dart' as ChartStyle;

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  double height = 100.0;
  double width = 150.0;

  static String value1;
  static String value2;
  static String value3;

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
      Color fillColor,
      FillPatternType fillPattern,
      Color strokeColor,
      double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);

    ChartStyle.TextStyle textStyle1 = ChartStyle.TextStyle();
    ChartStyle.TextStyle textStyle2 = ChartStyle.TextStyle();

    /*var paint = new Paint()
      ..color = Colors.red
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10;*/

    canvas.drawRRect(
      Rectangle(bounds.left - width / 2, bounds.top - height, width, height),
      fill: Color.white,
      stroke: charts.Color.black,
      patternColor: Color.black,
      fillPattern: FillPatternType.solid,
      strokeWidthPx: 10,
      patternStrokeWidthPx: 10,
      radius: 10.0,
      roundTopLeft: true,
      roundBottomLeft: true,
      roundBottomRight: true,
      roundTopRight: true,
    );

    // canvas.drawRect(
    //Rectangle(bounds.left - (width / 2) + 2, bounds.top - (height - 2),
    //  width - 4, height - 4),
    //fill: Color.white);

    canvas.drawPoint(
        point: Point(
          (bounds.left).round() - 55,
          (bounds.top - height + 48).round(),
        ),
        radius: 5,
        fill: Color.fromHex(code: "#0000FF"),
        stroke: strokeColor,
        strokeWidthPx: getSolidStrokeWidthPx(strokeWidthPx));

    canvas.drawPoint(
        point: Point(
          (bounds.left).round() - 55,
          (bounds.top - height + 77).round(),
        ),
        radius: 5,
        fill: Color.fromHex(code: "#00FF00"),
        stroke: strokeColor,
        strokeWidthPx: getSolidStrokeWidthPx(strokeWidthPx));

    //canvas.drawRect(
    // Rectangle(bounds.left - width / 2, bounds.top - height, width, height),
    //fill: Color.black);

    //canvas.drawCircle(new Offset(point.x, point.y), strokeWidthPx, paint);

    textStyle1.color = Color.black;
    textStyle1.fontSize = 15;

    textStyle2.color = Color.fromHex(code: "#85929E");
    textStyle2.fontSize = 15;

    //  1st text
    canvas.drawText(ChartText.TextElement("$value1", style: textStyle1),
        (bounds.left).round() - 40, (bounds.top - height + 10).round());

    //  2nd text
    canvas.drawText(ChartText.TextElement("$value2", style: textStyle1),
        (bounds.left).round() - 40, (bounds.top - height + 40).round());

    canvas.drawText(ChartText.TextElement("Income", style: textStyle2),
        (bounds.left).round() + 10, (bounds.top - height + 40).round());

    //  3rd text
    canvas.drawText(ChartText.TextElement("$value3", style: textStyle1),
        (bounds.left).round() - 40, (bounds.top - height + 70).round());

    canvas.drawText(ChartText.TextElement("Expense", style: textStyle2),
        (bounds.left).round() + 10, (bounds.top - height + 70).round());
  }
}
