import 'package:aitl/model/json/db_intr/dashboard/GetChartAPIModel.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class SimpleBarChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  SimpleBarChart(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory SimpleBarChart.withSampleData(
      List<CaseCountMonthlyWiseSummaryReports>
          _caseCountMonthlyWiseSummaryReports) {
    return new SimpleBarChart(
      _createSampleData(_caseCountMonthlyWiseSummaryReports),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      barGroupingType: charts.BarGroupingType.grouped,
      primaryMeasureAxis: charts.NumericAxisSpec(
        tickProviderSpec: charts.BasicNumericTickProviderSpec(
          dataIsInWholeNumbers: true,

          //desiredTickCount: 28,
          // zeroBound 设置为 false 时柱状图居然出错了，现象为会有部分画在了x轴的下面
          zeroBound: true,
        ),
      ),
      domainAxis: new charts.OrdinalAxisSpec(
        // Make sure that we draw the domain axis line.
        showAxisLine: false,
        /*renderSpec: charts.SmallTickRendererSpec(labelRotation: 60),*/
        // But don't draw anything else.
      ),
      //domainAxis: new charts.DateTimeAxisSpec(
      //  showAxisLine: false, renderSpec: new charts.NoneRenderSpec()),
      defaultRenderer: new charts.BarRendererConfig(
        maxBarWidthPx: 15,
        strokeWidthPx: 1.0,
        //stackedBarPaddingPx: 250,

        //weightPattern: [4, 10],
        //strokeWidthPx: 500,
        // groupingType: charts.BarGroupingType.groupedStacked,
        cornerStrategy: const charts.ConstCornerStrategy(50),
      ),
      /*behaviors: [
        new charts.SeriesLegend(
          // Positions for "start" and "end" will be left and right respectively
          // for widgets with a build context that has directionality ltr.
          // For rtl, "start" and "end" will be right and left respectively.
          // Since this example has directionality of ltr, the legend is
          // positioned on the right side of the chart.
          position: charts.BehaviorPosition.end,
          // For a legend that is positioned on the left or right of the chart,
          // setting the justification for [endDrawArea] is aligned to the
          // bottom of the chart draw area.
          outsideJustification: charts.OutsideJustification.endDrawArea,
          // By default, if the position of the chart is on the left or right of
          // the chart, [horizontalFirst] is set to false. This means that the
          // legend entries will grow as new rows first instead of a new column.
          horizontalFirst: false,
          // By setting this value to 2, the legend entries will grow up to two
          // rows before adding a new column.
          desiredMaxRows: 2,
          // This defines the padding around each legend entry.
          cellPadding: new EdgeInsets.only(right: 4.0, bottom: 4.0),
          // Render the legend entry text with custom styles.
          entryTextStyle: charts.TextStyleSpec(
              color: charts.Color(r: 127, g: 63, b: 191),
              fontFamily: 'Georgia',
              fontSize: 11),
        ),
      ],*/
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData(
      List<CaseCountMonthlyWiseSummaryReports>
          caseCountMonthlyWiseSummaryReports) {
    final List<OrdinalSales> listTotalCases = [];
    final List<OrdinalSales> listTotalCompletedCases = [];
    int j = 0;
    for (int i = 0; i < 12; i++) {
      try {
        CaseCountMonthlyWiseSummaryReports model;
        try {
          model = caseCountMonthlyWiseSummaryReports[j];
          if ((i + 1) == int.parse(model.dateOfMonth)) {
            j++;
            model = CaseCountMonthlyWiseSummaryReports.fromJson({
              "DateOfMonth": (i + 1).toString(),
              "TotalCase": model.totalCase,
              "TotalCompletedCase": model.totalCompletedCase
            });
            listTotalCases.add(new OrdinalSales(
                DateFun.getMonthByDigit(index: i + 1), model.totalCase));
            listTotalCompletedCases.add(new OrdinalSales(
                DateFun.getMonthByDigit(index: i + 1),
                model.totalCompletedCase));
          } else {
            model = CaseCountMonthlyWiseSummaryReports.fromJson({
              "DateOfMonth": (i + 1).toString(),
              "TotalCase": 0,
              "TotalCompletedCase": 0
            });
            listTotalCases.add(
                new OrdinalSales(DateFun.getMonthByDigit(index: i + 1), 0));
            listTotalCompletedCases.add(
                new OrdinalSales(DateFun.getMonthByDigit(index: i + 1), 0));
          }
        } catch (e) {
          model = CaseCountMonthlyWiseSummaryReports.fromJson({
            "DateOfMonth": (i + 1).toString(),
            "TotalCase": 0,
            "TotalCompletedCase": 0
          });
          listTotalCases
              .add(new OrdinalSales(DateFun.getMonthByDigit(index: i + 1), 0));
          listTotalCompletedCases
              .add(new OrdinalSales(DateFun.getMonthByDigit(index: i + 1), 0));
        }
      } catch (e) {}
    }

    print(listTotalCases);
    print(listTotalCompletedCases);

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Total Cases',
        colorFn: (_, __) => charts.Color.fromHex(code: '#C1272D'),
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: listTotalCases,
      ),
      new charts.Series<OrdinalSales, String>(
        id: 'Total Completed Case',
        colorFn: (_, __) => charts.Color.fromHex(code: '#CDCDCD'),
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: listTotalCompletedCases,
      ),
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
