import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/config/db_intro/intro_cfg.dart';
import 'package:aitl/controller/api/db_intr/leadfetch/LeadCaseAPIMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_intr/createlead/ResolutionModel.dart';
import 'package:aitl/view/db_intr/createlead/LeadBase.dart';
import 'package:aitl/view/db_intr/createlead/LeadDetails.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../controller/network/NetworkMgr.dart';
import '../../../model/json/db_intr/createlead/CaseOwnerAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetAllResSupportAdminAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetTwilioPhoneNumberAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetUserTaskCatAPIModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../../view_model/helper/ui_helper.dart';
import '../../widgets/dropdown/custom_dd.dart';
import '../../widgets/images/MyNetworkImage.dart';
import '../../widgets/progress/AppbarBotProgbar.dart';
import '../../widgets/txt/Txt.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';

class LeadsScreen extends StatefulWidget {
  const LeadsScreen({Key key}) : super(key: key);

  @override
  State createState() => _LeadsScreenState();
}

class _LeadsScreenState extends BaseLead<LeadsScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<ResolutionModel> listResolutions = [];
  List<dynamic> listAllSupAdminResolutions = [];
  List<UserTaskCategorys> userTaskCategorys = [];
  List<Users> listOwnerName;
  String twilioPhoneNumber = '';

  int pageStart = 1;
  int pageCount = AppConfig.page_limit;
  bool isPageDone = false;
  bool isLoading = false;

  caseOwnerAPI() async {
    try {
      ddCaseOwner =
          DropListModel([OptionItem(id: "0", title: selectCaseOwner)]);
      optCaseOwner = OptionItem(id: "0", title: selectCaseOwner);
      var url = ServerIntr
          .GET_USERBYCOMMUNITYID_COMPUSERID_FOR_SELECT_OPT_ADVISOR_URL;
      url = url.replaceAll("#communityId#", userData.userModel.communityID);
      url = url.replaceAll(
          "#userCompanyId#", userData.userModel.userCompanyID.toString());
      await APIViewModel().req<CaseOwnerAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                listOwnerName = model.responseData.users;
                for (final caseOwner in listOwnerName) {
                  if (IntroCfg.listCaseOwnerCommunityID
                          .contains(caseOwner.communityId) &&
                      caseOwner.name != null) {
                    ddCaseOwner.listOptionItems.add(OptionItem(
                        id: caseOwner.id.toString(), title: caseOwner.name));
                  }
                }
              }
            }
          });
    } catch (e) {}
  }

  getUserTaskCategoryAPI() async {
    try {
      await APIViewModel().req<GetUserTaskCatAPIModel>(
          context: context,
          url: ServerIntr.GET_USER_TASK_CATEGORY_URL,
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                userTaskCategorys = model.responseData.userTaskCategorys;
              }
            }
          });
    } catch (e) {}
  }

  geTwilioPhoneNumberAPI() async {
    try {
      await APIViewModel().req<GetTwilioPhoneNumberAPIModel>(
          context: context,
          url: ServerIntr.GET_TWILIO_PHONENUMBER_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                twilioPhoneNumber = model.responseData.twilioPhoneNumber;
              }
            }
          });
    } catch (e) {}
  }

  geAllResSupAdminIDAPI() async {
    try {
      var url = ServerIntr.GET_ALL_RES_SUPPORT_ADMINID_URL;
      url =
          url.replaceAll("#SupportAdminId#", userData.userModel.id.toString());
      url = url.replaceAll("#IsLockAutoCall#", "1");
      await APIViewModel().req<GetAllResSupportAdminAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                listAllSupAdminResolutions = model.responseData.resolutions;
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listResolutions = null;
    super.dispose();
  }

  @override
  doSearch(eSearch v) {
    switch (v) {
      case eSearch.Stage:
        break;
      case eSearch.Status:
        break;
      case eSearch.DueDate:
        break;
      case eSearch.Search:
        break;
      default:
    }
    refreshData();
  }

  appInit() async {
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        wBox = getW(context) / 3.5;
      });
      await caseOwnerAPI();
      await refreshData();
      await getUserTaskCategoryAPI();
      await geTwilioPhoneNumberAPI();
      await geAllResSupAdminIDAPI();
      setState(() {});
    } catch (e) {}
  }

  reset() async {
    selectedStage = "Stage".obs;
    selectedStatus = "Status".obs;
    optCaseOwner = OptionItem(id: null, title: "Select case owner");
    selectedDateTime = "12 Hours";
    selectedSortBy = "Task Due Date";
    searchByTxt.clear();
    await refreshData();
  }

  Future<void> refreshData() async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() {
        pageStart = 1;
        isPageDone = false;
        isLoading = true;
        listResolutions.clear();
      });
      await onLazyLoadAPI();
    });
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });

      if (date1 == '' && date2 == '') {
        date1 = DateFormat('dd-MMM-yyyy HH:mm')
            .format(DateTime.now().subtract(new Duration(days: 7)));
        date2 = DateFormat('dd-MMM-yyyy HH:mm').format(DateTime.now());
      }
      final param = {
        'UserCompanyId': userData.userModel.userCompanyID.toString(),
        'Criteria': '1',
        'AssigneeId': optCaseOwner.id != null ? optCaseOwner.id : '0',
        'Status': selectedStatus.value != "All" ? selectedStatus.value : "",
        'IsSpecificDate': "",
        'FromDateTime': date1,
        'ToDateTime': date2,
        'InitiatorId': '0',
        'Title': '',
        'Stage': selectedStage.value != "None" ? selectedStage.value : "",
        'LeadStatus': '',
        'Page': pageStart.toString(),
        'Count': pageCount.toString(),
        'SearchText': searchByTxt.text.trim(),
        'OrderBy': selectedSortBy != "None" ? selectedSortBy : "",
        'ResolutionType': "All",
        'DateTimeSelectOption': selectedDateTime
      };
      print(param);
      await LeadCaseAPIMgr().getLeadCase(
          context: context,
          param: param
          /*{
            'Status': listStatus[manageLeadController.statusRB.value],
            'Title': "",
            'Description': "",
            'Remarks': "",
            'InitiatorId': "0",
            'ServiceDate': "",
            'ResolutionType': "All",
            'ParentId': "0",
            'Page': pageStart.toString(),
            'Count': pageCount.toString(),
            'AssigneeId': "0",
            'FromDateTime': date1,
            'ToDateTime': date2,
            'Criteria': "1",
            'TaskStatus': "901",
            'UserCompanyId': userData.userModel.userCompanyID.toString(),
            'LeadStatus': "",
            'Stage': stage != "None" ? stage : "",
            'OrderBy': orderBy != "None" ? orderBy : "",
            'SearchText': "",
            'IsSpecificDate': "",
            'DateTimeSelectOption':
                (selectedDateTime != customDateTime) ? selectedDateTime : '',
          }*/
          ,
          callback: (model) {
            if (mounted && model != null) {
              if (model.success) {
                final List<ResolutionModel> resolutions =
                    model.responseData.resolutions;
                if (resolutions != null) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (resolutions.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (ResolutionModel res in resolutions) {
                      listResolutions.add(res);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                  setState(() {
                    isLoading = false;
                  });
                } else {
                  setState(() {
                    isLoading = false;
                  });
                }
              } else {
                listResolutions = [];
              }
              setState(() {});
            }
          });
    }
    if (mounted) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        endDrawer: new Drawer(
            backgroundColor: MyTheme.bgColor2,
            elevation:
                5, // set elevation to 0, otherwise you see the transparent drawer a little bit
            child: openDrawerFilter()),
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.titleColor,
          title: UIHelper().drawAppbarTitle(title: "Manage Leads"),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  reset();
                },
                icon: Icon(Icons.refresh_outlined, color: Colors.white))
          ],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return NotificationListener(
      onNotification: (scrollNotification) {
        if (scrollNotification is ScrollStartNotification) {
          //print('Widget has started scrolling');
        } else if (scrollNotification is ScrollEndNotification) {
          if (!isPageDone) {
            pageStart++;
            onLazyLoadAPI();
          }
        }
        return true;
      },
      child: RefreshIndicator(
        color: Colors.white,
        backgroundColor: MyTheme.brandColor,
        onRefresh: refreshData,
        child: SingleChildScrollView(
          primary: true,
          child: Column(
            children: [
              //drawSearchHeader(),
              drawSearchHeader(),
              (listResolutions.length > 0)
                  ? ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      primary: false,
                      itemCount: listResolutions.length,
                      itemBuilder: (BuildContext context, int index) {
                        return drawItems(listResolutions[index]);
                      },
                    )
                  : (isLoading)
                      ? CircularProgressIndicator()
                      : drawNF(),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawItems(ResolutionModel resolutions) {
    var stageColor = MyTheme.titleColor;
    try {
      stageColor = IntroCfg.stageColor[resolutions.stage];
    } catch (e) {}

    final name = (resolutions.firstName ?? '') +
        " " +
        (resolutions.middleName ?? '') +
        " " +
        (resolutions.lastName ?? '');

    return GestureDetector(
      onTap: () {
        Get.to(() => LeadDetails(
              resolution: resolutions,
            )).then((value) => refreshData());
      },
      child: (resolutions != null)
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Card(
                  elevation: 5,
                  //color: Colors.transparent,
                  child: IntrinsicHeight(
                    child: Row(
                      children: [
                        Container(width: getWP(context, .5), color: stageColor),
                        SizedBox(width: 10),
                        Container(
                          width: getWP(context, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 10),
                              Container(
                                width: getWP(context, 15),
                                height: getWP(context, 15),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: MyNetworkImage.loadProfileImage(
                                        resolutions.profileImageUrl),
                                    fit: BoxFit.cover,
                                  ),
                                  shape: BoxShape.circle,
                                ),
                              ),
                              SizedBox(height: 5),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey.shade300,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(2),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(width: 5),
                                      Txt(
                                          txt: "Stage",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .5,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                      drawCircle(
                                          context: context,
                                          color: stageColor,
                                          size: 4),
                                      SizedBox(width: 2),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: Container(
                            width: getWP(context, 79),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Txt(
                                    txt: name ?? '',
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: true),
                                SizedBox(height: 2),
                                Txt(
                                    txt: resolutions.resolutionType ?? '',
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: true),
                                SizedBox(height: 2),
                                Txt(
                                    txt: resolutions.title ?? '',
                                    txtColor: Colors.red,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                                SizedBox(height: 2),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.email,
                                      color: Colors.grey,
                                      size: 15,
                                    ),
                                    SizedBox(width: 5),
                                    Flexible(
                                      child: Txt(
                                          txt: resolutions.emailAddress ?? '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 2),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.phone,
                                      color: Colors.grey,
                                      size: 15,
                                    ),
                                    SizedBox(width: 5),
                                    Flexible(
                                      child: Txt(
                                          txt: resolutions.phoneNumber ?? '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 2),
                                Row(
                                  children: [
                                    Flexible(
                                      child: Txt(
                                          txt: resolutions.stage ?? '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                    SizedBox(width: 5),
                                    Icon(Icons.watch_later_rounded,
                                        color: Colors.grey.shade400, size: 20),
                                    SizedBox(width: 5),
                                    Flexible(
                                      child: Txt(
                                          txt: DateFun.getDate(
                                                  resolutions.creationDate,
                                                  "kk:mm, dd MMM yyyy") ??
                                              '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          : SizedBox(),
    );
  }

  drawSearchHeader() {
    //final wBox = (getW(context) / 5) - 50;
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        width: getW(context),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  fit: FlexFit.tight,
                  flex: 2,
                  child: Obx(
                    () => DDList(
                      list: itemsStage,
                      hint: selectedStage.value == 'None'
                          ? "Stage"
                          : selectedStage.value,
                      btnText: "Search",
                      onItemChange: (k, v) {
                        selectedStage.value = v;
                      },
                      onClicked: () {
                        refreshData();
                      },
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Flexible(
                  flex: 2,
                  child: Obx(
                    () => DDList(
                      list: itemsStatus,
                      hint: selectedStatus.value == 'All'
                          ? "Status"
                          : selectedStatus.value,
                      btnText: "Search",
                      onItemChange: (k, v) {
                        selectedStatus.value = v;
                      },
                      onClicked: () {
                        refreshData();
                      },
                    ),
                  ),
                ),
                //SizedBox(width: 5),
                Flexible(
                    child: GestureDetector(
                  onTap: () {
                    _scaffoldKey.currentState.openEndDrawer();
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Flexible(
                          child: Image.asset("assets/images/ico/filter_ico.png",
                              height: 30, width: 15)),
                      SizedBox(width: 3),
                      Flexible(
                          child: Text(
                        "Filter",
                        style: TextStyle(color: Colors.black, fontSize: 14),
                      ))
                    ],
                  ),
                )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
