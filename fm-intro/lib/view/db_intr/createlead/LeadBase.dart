import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../widgets/txt/Txt.dart';
import 'package:intl/intl.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

enum eSearch {
  Stage,
  Status,
  DueDate,
  Search,
}

abstract class BaseLead<T extends StatefulWidget> extends State<T> with Mixin {
  reset();
  refreshData();
  doSearch(eSearch v);

  final searchByTxt = TextEditingController();

  final DateTime dateNow = DateTime.now();

  var date1 = "";
  var date2 = "";
  var time1 = TimeOfDay.now();
  var time2 = TimeOfDay.now();

  var datelast;
  var datefirst;
  var datelast2;
  var datefirst2;
  final customDateTime = "Custom Date Time";

  //  list dropdown case owner
  final selectCaseOwner = "Select case owner";
  DropListModel ddCaseOwner = DropListModel([]);
  OptionItem optCaseOwner;

  double wBox;
  String selectedDateTime = "12 Hours";
  final itemsDateTime = {
    0: "12 Hours",
    1: "24 Hours",
    2: "2 Days",
    3: "7 Days",
    4: "Custom Date Time",
    5: "All Dates",
  };

  String selectedSortBy = "Task Due Date";
  final itemsSortBy = {
    0: "Task Due Date",
    1: "Last Update Date",
    2: "My Last Call",
    3: "My Last Email",
    4: "My Last Text",
    5: "Last Creation Date",
    6: "Name",
    7: "None"
  };

  var selectedStage = "Stage".obs;
  final itemsStage = {
    0: "Added",
    1: "Qualifying",
    2: "Processing",
    3: "Converted",
    4: "Junk",
    5: "None",
  };

  var selectedStatus = "Status".obs;
  final itemsStatus = {
    0: "All",
    1: "Converted to Customer",
    2: "Call back later",
    3: "Attempt to contact",
    4: "Needs information",
    5: "Wait for now",
    6: "Unsolicited call",
    7: "Do not contact again",
    8: "Not found property yet",
    9: "No longer proceeding",
    10: "Changed circumstances",
    11: "Wrong number",
    12: "Gone elsewhere",
    13: "Lost to competition",
    14: "Deleted",
  };

  drawNF() {
    return Container(
      width: getW(context),
      height: getH(context) / 2,
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        //shrinkWrap: true,
        children: [
          Container(
            width: getWP(context, 50),
            height: getWP(context, 30),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                    "assets/images/screens/db_cus/my_cases/case_nf2.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          //SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Container(
              child: Txt(
                txt: "Manage listing not found.\nTry with other options",
                txtColor: MyTheme.mycasesNFBtnColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
                //txtLineSpace: 1.5,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> drawTimePicker(TimeOfDay t, Function(TimeOfDay) callback) async {
    final picked_s = await showTimePicker(
        context: context,
        initialTime: t,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light(
                // change the border color
                primary: MyTheme.bgColor,
                // change the text color
                onSurface: Colors.black,
              ),
              // button colors
              buttonTheme: ButtonThemeData(
                colorScheme: ColorScheme.light(
                  primary: Colors.black,
                ),
              ),
            ),
            child: child,
          );
        });
    if (picked_s != null && picked_s != t) {
      callback(picked_s);
    }
  }

  drawCustomDateTime() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(height: 5),
          _drawCusDateRow("From", date1, datefirst, datelast, datefirst,
              (DateTime d) {
            setState(() {
              try {
                time1 = TimeOfDay.now();
                date1 = DateFormat('dd-MMM-yyyy HH:mm').format(d).toString();
                drawTimePicker(time1, (t) {
                  final dd = DateTime(d.year, d.month, d.day, t.hour, t.minute);
                  date1 = DateFormat('dd-MMM-yyyy HH:mm').format(dd).toString();
                  print(t);
                });
              } catch (e) {
                myLog(e.toString());
              }
            });
          }),
          SizedBox(height: 3),
          _drawCusDateRow("To", date2, datefirst2, datelast2, DateTime.now(),
              (d) {
            setState(() {
              try {
                time2 = TimeOfDay.now();
                date2 = DateFormat('dd-MMM-yyyy HH:mm').format(d).toString();
                drawTimePicker(time2, (t) {
                  final dd = DateTime(d.year, d.month, d.day, t.hour, t.minute);
                  date2 = DateFormat('dd-MMM-yyyy HH:mm').format(dd).toString();
                  print(t);
                });
              } catch (e) {
                myLog(e.toString());
              }
            });
          }),
        ],
      ),
    );
  }

  _drawCusDateRow(String title, String dLabel, fdate, ldate, initialDate,
      Function(DateTime) callback) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: getWP(context, 25),
          decoration: BoxDecoration(
              color: Colors.grey.shade200,
              border: Border.all(
                color: Colors.black26,
              ),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                bottomLeft: Radius.circular(5),
              )),
          child: Padding(
            padding: const EdgeInsets.all(9),
            child: Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
        Expanded(
          flex: 3,
          child: GestureDetector(
            onTap: () {
              showDatePicker(
                context: context,
                initialDate: initialDate,
                firstDate: fdate,
                lastDate: ldate,
                fieldHintText: "dd/mm/yyyy",
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme: ColorScheme.light(
                        // change the border color
                        primary: MyTheme.bgColor,
                        // change the text color
                        onSurface: Colors.black,
                      ),
                      // button colors
                      buttonTheme: ButtonThemeData(
                        colorScheme: ColorScheme.light(
                          primary: Colors.black,
                        ),
                      ),
                    ),
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: Container(
              width: getW(context),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: .5),
                  borderRadius: BorderRadius.all(Radius.circular(0))),
              child: Padding(
                padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text(
                  dLabel,
                  style: TextStyle(color: Colors.black, fontSize: 12),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  openDrawerFilter() {
    return Container(
      //height: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: ListView(
              primary: true,
              shrinkWrap: true,
              children: [
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    'Search filter',
                    style: TextStyle(fontSize: 16, color: MyTheme.inputColor),
                  ),
                ),
                SizedBox(height: 10),
                DropDownListDialog(
                  context: context,
                  h1: 'Choose Case owner',
                  txtSize: 1.8,
                  title: optCaseOwner.title,
                  ddTitleList: ddCaseOwner,
                  radius: 5,
                  vPadding: 1,
                  callback: (optionItem) {
                    optCaseOwner = optionItem;
                    setState(() {});
                  },
                ),
                SizedBox(height: 10),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: getWP(context, 25),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade200,
                          border: Border.all(
                            color: Colors.black26,
                          ),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5),
                            bottomLeft: Radius.circular(5),
                          )),
                      child: Padding(
                        padding: const EdgeInsets.all(9),
                        child: Txt(
                            txt: "Sort by",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton2(
                          buttonHeight: 37,
                          buttonWidth: getW(context),
                          buttonPadding:
                              const EdgeInsets.only(left: 14, right: 14),
                          buttonDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0),
                            border: Border.all(
                              color: Colors.black26,
                            ),
                            color: MyTheme.bgColor2,
                          ),
                          dropdownDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: MyTheme.bgColor2,
                          ),
                          iconEnabledColor: Colors.black,
                          iconDisabledColor: Colors.grey,
                          hint: Text(
                            'Task Due Date',
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                            ),
                          ),
                          items: itemsSortBy.entries
                              .map((v) => DropdownMenuItem<String>(
                                    value: v.value,
                                    child: Text(
                                      v.value,
                                      style: const TextStyle(
                                        fontSize: 12,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ))
                              .toList(),
                          value: selectedSortBy,
                          onChanged: (value) {
                            selectedSortBy = value;
                            setState(() {});
                          },
                          //itemHeight: 40,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Container(
                      width: getWP(context, 25),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade200,
                          border: Border.all(
                            color: Colors.black26,
                          ),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5),
                            bottomLeft: Radius.circular(5),
                          )),
                      child: Padding(
                        padding: const EdgeInsets.all(9),
                        child: Txt(
                            txt: "Search",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                    Expanded(
                        flex: 3,
                        child: TextField(
                          controller: searchByTxt,
                          style: TextStyle(color: Colors.black, fontSize: 13),
                          decoration: new InputDecoration(
                            isDense: true,
                            counterText: "",
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 10),
                            /*suffixIcon: searchByTxt.text.length > 0
                                ? GestureDetector(
                                    onTap: () {
                                      searchByTxt.clear();
                                    },
                                    child: Icon(Icons.close,
                                        color: Colors.black, size: 15))
                                : null,*/
                            hintText: "Lead id/Name/Email/Phone Number etc",
                            hintStyle:
                                TextStyle(color: Colors.grey, fontSize: 13),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(5),
                                bottomRight: Radius.circular(5),
                              ),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(5),
                                bottomRight: Radius.circular(5),
                              ),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                          ),
                        )),
                  ],
                ),
                //  ******************************************************
                SizedBox(height: 10),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: getWP(context, 25),
                            decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                border: Border.all(
                                  color: Colors.black26,
                                ),
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  bottomLeft: Radius.circular(5),
                                )),
                            child: Padding(
                              padding: const EdgeInsets.all(9),
                              child: Txt(
                                  txt: "Date Time",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                          ),
                          //SizedBox(width: 10),
                          Expanded(
                            flex: 5,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                buttonHeight: 37,
                                buttonWidth: getW(context),
                                buttonPadding:
                                    const EdgeInsets.only(left: 14, right: 14),
                                buttonDecoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(0),
                                  border: Border.all(
                                    color: Colors.black26,
                                  ),
                                  color: MyTheme.bgColor2,
                                ),
                                dropdownDecoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: MyTheme.bgColor2,
                                ),
                                iconEnabledColor: Colors.black,
                                iconDisabledColor: Colors.grey,
                                hint: Text(
                                  '12 Hours',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                  ),
                                ),
                                items: itemsDateTime.entries
                                    .map((v) => DropdownMenuItem<String>(
                                          value: v.value,
                                          child: Text(
                                            v.value,
                                            style: const TextStyle(
                                              fontSize: 12,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ))
                                    .toList(),
                                value: selectedDateTime,
                                onChanged: (value) {
                                  selectedDateTime = value;
                                  setState(() {
                                    if (value != customDateTime) {
                                      date1 = "";
                                      date2 = "";
                                      if (value == "12 Hours") {
                                        date1 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now().subtract(
                                                new Duration(hours: 12)));
                                        date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now());
                                      } else if (value == "24 Hours") {
                                        date1 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now().subtract(
                                                new Duration(days: 1)));
                                        date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now());
                                      } else if (value == "2 Days") {
                                        date1 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now().subtract(
                                                new Duration(days: 2)));
                                        date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now());
                                      } else if (value == "7 Days") {
                                        date1 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now().subtract(
                                                new Duration(days: 7)));
                                        date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now());
                                      } else if (value == "All Dates") {
                                        date1 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now().subtract(
                                                new Duration(days: 365)));
                                        date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                            .format(DateTime.now());
                                      }
                                    } else {
                                      datelast = DateTime(
                                          dateNow.year,
                                          dateNow.month,
                                          dateNow.day,
                                          dateNow.hour,
                                          dateNow.minute);
                                      datefirst = DateTime(
                                          dateNow.year - 10,
                                          dateNow.month,
                                          dateNow.day - 7,
                                          dateNow.hour,
                                          dateNow.minute);
                                      datelast2 = DateTime(
                                          dateNow.year,
                                          dateNow.month,
                                          dateNow.day,
                                          dateNow.hour,
                                          dateNow.minute);
                                      datefirst2 = DateTime(
                                          dateNow.year - 10,
                                          dateNow.month,
                                          dateNow.day,
                                          dateNow.hour,
                                          dateNow.minute);
                                      date1 = DateFormat('dd-MMM-yyyy HH:mm')
                                          .format(DateTime.now()
                                              .subtract(new Duration(days: 7)));
                                      date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                          .format(DateTime.now());
                                    }
                                  });
                                },
                                //itemHeight: 40,
                              ),
                            ),
                          ),
                        ],
                      ),
                      (selectedDateTime == customDateTime)
                          ? drawCustomDateTime()
                          : SizedBox(),
                    ]),

                //  *****************************************************************
              ],
            ),
          ),
          SizedBox(height: 10),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 1, left: 1, right: 1),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Flexible(
                      child: GestureDetector(
                    onTap: () async {
                      Navigator.pop(context);
                      reset();
                    },
                    child: Container(
                      color: Colors.grey,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.all(15),
                        child: const Text(
                          "Reset",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )),
                  Flexible(
                      child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      doSearch(eSearch.Search);
                    },
                    child: Container(
                      color: Colors.redAccent,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.all(15),
                        child: const Text(
                          "Search",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
