import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/auth/UserModel.dart';
import 'package:aitl/view/db_intr/intro_mixin.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../config/Server.dart';
import '../../../config/db_cus/NewCaseCfg.dart';
import '../../../config/db_intro/intro_cfg.dart';
import '../../../controller/api/db_cus/new_case/PostCaseAPIMgr.dart';
import '../../../controller/form_validator/UserProfileVal.dart';
import '../../../controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import '../../../controller/helper/db_cus/tab_newcase/PostNewCaseHelper.dart';
import '../../../controller/network/NetworkMgr.dart';
import '../../../model/json/db_cus/tab_newcase/LocationsModel.dart';
import '../../../model/json/db_intr/createlead/GetCaseTypeByUserIdAPIModel.dart';
import '../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../model/json/db_intr/createlead/UserDetailsDataAutoSuggAPIModel.dart';
import '../../../model/json/misc/CommonAPIModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../widgets/btn/Btn.dart';
import '../../widgets/btn/MMBtn.dart';
import '../../widgets/dialog/DatePickerView.dart';
import '../../widgets/dropdown/DropDownListDialog.dart';
import '../../widgets/dropdown/DropListModel.dart';
import '../../widgets/images/MyNetworkImage.dart';
import '../../widgets/input/InputTitleBox.dart';
import '../../widgets/input/drawInputCurrencyBox.dart';
import '../../widgets/progress/AppbarBotProgbar.dart';
import '../../widgets/radio/draw_radio_group.dart';
import '../../widgets/txt/Txt.dart';
import '../../widgets/webview/WebScreen.dart';
import '../createlead/leaddetails/base_lead_case.dart';
import 'CreateLeadProfile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:stream_transform/stream_transform.dart';

class CreateLeadCase extends StatefulWidget {
  final ResolutionModel resolutions;
  final UserModel user;
  final bool isNewCase;
  const CreateLeadCase(
      {Key key, this.resolutions, this.user, this.isNewCase = false})
      : super(key: key);

  @override
  State createState() => _CreateLeadCaseState();
}

class _CreateLeadCaseState extends BaseLeadCase<CreateLeadCase> {
  bool isLoading = false;
  var subTitle;
  String whenPayAble = "Select..";
  String whenAnotherPayAble = "Select..";

  final title = TextEditingController();
  final searchController = TextEditingController();
  final chargingFeePayAbleController = TextEditingController();
  final anotherChargingFeePayAbleController = TextEditingController();
  final companyNameController = TextEditingController();
  final registerAddressController = TextEditingController();
  final registerNumberController = TextEditingController();
  final emailController = TextEditingController();
  final leadNotesController = TextEditingController();
  final estimatedEarningController = TextEditingController();
  final referenceController = TextEditingController();

  final focusTitle = FocusNode();
  final focusSearch = FocusNode();

  bool isProductTypeRegular = true;
  bool isSPVCompany = false;
  bool isTradingCompany = false;
  bool isClientAuthority = false;
  int caseClassificationIndex = 1;

  var caseTypeMap = {};

  final paymentMethodList = ["Application", "Offer", "Completion"];

  final DateTime dateNow = DateTime.now();

  String regDate = "";

  ResolutionModel resolutions;
  UserModel user;
  UserCaseTypePermission userCaseTypePermission;
  List<UserDetailsDatas> listUserDetailsDatasAutoSug = [];
  UserDetailsDatas userDetailsDatas;

  final ScrollController scrollController = ScrollController();
  final ExpandableController expandableController = ExpandableController();
  final StreamController<String> streamController = StreamController();

  int caseIndex = -1;
  DropListModel ddCaseType;
  OptionItem optCaseType;

  bool isddCaseSubTypeTextField = false;
  DropListModel ddCaseSubType;
  OptionItem optCaseSubType = OptionItem(id: null, title: "Select");

  DropListModel ddDisclosureMethod = DropListModel([
    OptionItem(id: "1", title: "Face to face"),
    OptionItem(id: "2", title: "Email"),
    OptionItem(id: "3", title: "Post"),
    OptionItem(id: "4", title: "Verbal"),
  ]);
  OptionItem optDisclosureMethod = OptionItem(id: null, title: "Select");

  DropListModel ddAdviceType = DropListModel([
    OptionItem(id: "1", title: "Face to face"),
    OptionItem(id: "2", title: "Non- Face to face"),
    OptionItem(id: "3", title: "Virtual face to face (Video Conferencing)"),
    OptionItem(id: "4", title: "Execution only"),
    OptionItem(id: "5", title: "Rejected advice"),
  ]);
  OptionItem optAdviceType = OptionItem(id: null, title: "Select");

  final listCaseClassification = {
    0: 'Advised case (Introducer advice)',
    1: 'Non advised case (Brightstar Advice)'
  };

  @override
  void initState() {
    try {
      appInit();
    } catch (e) {}
    super.initState();
  }

  @override
  void dispose() {
    resolutions = null;
    user = null;
    userCaseTypePermission = null;
    listUserDetailsDatasAutoSug = null;
    userDetailsDatas = null;
    scrollController.dispose();
    expandableController.dispose();
    streamController.close();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      resolutions = widget.resolutions;
      user = widget.user;

      try {
        caseTypeMap = NewCaseCfg().getCaseType(resolutions.title);
      } catch (e) {}

      setUserDatas();

      try {
        searchController.text = user.firstName + ' ' + user.lastName;
      } catch (e) {}

      streamController.stream
          .debounce(Duration(seconds: 1))
          .listen((s) => _validateValues());

      if (!widget.isNewCase) {
        await APIViewModel().req<GetCaseTypeByUserIdAPIModel>(
            context: context,
            url: ServerIntr.GET_CASE_TYPE_USERID_URL
                .replaceAll("#userId#", user.userCompanyID.toString()),
            reqType: ReqType.Get,
            callback: (model) async {
              if (mounted && model != null) {
                if (model.success) {
                  userCaseTypePermission =
                      model.responseData.userCaseTypePermission;
                  setState(() {});
                } else {}
              }
            });
      }

      /*await caseOwnerAPI((listOwnerName) {
        ddCaseOwner = DropListModel([]);
        optCaseOwner = OptionItem(id: null, title: "Select case owner");
        for (final caseOwner in listOwnerName) {
          if (IntroCfg.listCaseOwnerCommunityID
                  .contains(caseOwner.communityId) &&
              caseOwner.name != null) {
            ddCaseOwner.listOptionItems.add(
                OptionItem(id: caseOwner.id.toString(), title: caseOwner.name));
          }
        }
        setState(() {});
      });*/

      try {
        if (!widget.isNewCase) {
          optCaseType = OptionItem(
              id: caseTypeMap['index'].toString(), title: caseTypeMap['title']);
        } else {
          optCaseType = OptionItem(id: null, title: "I'm looking for...");
        }
        final List<OptionItem> lst = [];
        for (var map in NewCaseCfg.listCreateNewCase) {
          lst.add(OptionItem(id: map["index"].toString(), title: map['title']));
        }
        ddCaseType = DropListModel(lst);
        if (!widget.isNewCase) {
          reloadCaseSubType(caseTypeMap['index']);
        } else {
          reloadCaseSubType(1);
        }
      } catch (e) {}

      setState(() {});
    } catch (e) {}
  }

  reloadCaseSubType(int index) {
    for (var map in NewCaseCfg.listCreateNewCase) {
      if (map['index'] == index) {
        List subList = map['subItem'];
        if (subList.length > 0) {
          if (!widget.isNewCase) {
            title.text = resolutions.description;
            optCaseSubType =
                OptionItem(id: "0", title: resolutions.description);
          } else {
            title.clear();
            optCaseSubType = OptionItem(id: null, title: "Select");
          }
          List<OptionItem> lst = [];
          for (final subMap in subList) {
            lst.add(OptionItem(
                id: subMap['index'].toString(), title: subMap['title']));
          }
          ddCaseSubType = DropListModel(lst);
          isddCaseSubTypeTextField = false;
          lst = null;
        } else {
          if (map['title'] == 'Others')
            isddCaseSubTypeTextField = true;
          else
            isddCaseSubTypeTextField = false;
          ddCaseSubType = DropListModel([]);
        }
        break;
      }
    }
  }

  resetData() {
    if (mounted) {
      isLoading = false;
      whenPayAble = "Select..";
      whenAnotherPayAble = "Select..";

      userDetailsDatas = null;

      optCaseType = OptionItem(id: null, title: "I'm looking for...");
      optCaseSubType = OptionItem(id: null, title: "Select");

      //optCaseOwner = OptionItem(id: null, title: "Select case owner");

      optDisclosureMethod = OptionItem(id: null, title: "Select");
      optAdviceType = OptionItem(id: null, title: "Select");

      title.clear();
      searchController.clear();
      chargingFeePayAbleController.clear();
      anotherChargingFeePayAbleController.clear();
      companyNameController.clear();
      registerAddressController.clear();
      registerNumberController.clear();
      emailController.clear();
      leadNotesController.clear();
      estimatedEarningController.clear();
      referenceController.clear();
      isProductTypeRegular = true;
      isSPVCompany = false;
      isTradingCompany = false;
      isClientAuthority = false;
      caseTypeMap = {};
      regDate = "";
      caseIndex = -1;
      isddCaseSubTypeTextField = false;
      setState(() {});
    }
  }

  setUserDatas() {
    try {
      userDetailsDatas = UserDetailsDatas.fromJson({
        'Id': user.id,
        'Name': user.name ?? '',
        'Status': 0,
        'FirstName': user.firstName,
        'LastName': user.lastName,
        'Address': user.address ?? '',
        'MiddleName': user.middleName ?? '',
        'UserName': user.userName ?? '',
        'Email': user.email ?? '',
        'MobileNumber': user.mobileNumber ?? '',
        'NamePrefix': user.namePrefix ?? '',
        'LastLoginDate': user.lastLoginDate ?? '',
        'DateUpdated': '',
        'CommunityId': "1",
        'CommunityName': '',
        'ImageServerUrl': '',
        'ThumbnailPath': '',
        'ProfileImageUrl': user.profileImageURL ?? '',
        'UserCompanyId': user.userCompanyID ?? resolutions.userCompanyId,
        'CompanyName': '',
        'Active': 0,
        'LastLoginDateUtc': '',
        'LastLoginDateLocal': '',
        'CreatedUser': '',
        'IsFirstLogin': 0,
        'StanfordWorkplaceURL': '',
        'GroupName': '',
        'PublishDate': '',
        'UnreadCount': 0,
        'DepartmentName': '',
        'BrokerCompanyName': '',
      });
      listUserDetailsDatasAutoSug.add(userDetailsDatas);
    } catch (e) {}
  }

  //function I am using to perform some logic
  _validateValues() async {
    final fromDateTime = DateFormat('dd-MMM-yyyy')
        .format(DateTime.now().subtract(new Duration(days: 7)));
    final toDateTime = DateFormat('dd-MMM-yyyy').format(DateTime.now());
    for (final m in listUserDetailsDatasAutoSug) {
      final txt = m.firstName + ' ' + m.lastName;
      if (searchController.text.startsWith(txt)) return;
    }
    if (searchController.text.length > 2 && !isLoading) {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        try {
          var url = ServerIntr.GET_USERDETAILS_AUTOSUG;
          if (user != null) {
            url.replaceAll("#communityId#", user.communityID.toString());
          } else {
            url = url.replaceAll(
                "#communityId#",
                !widget.isNewCase
                    ? user.communityID.toString()
                    : "1"); //userData.userModel.communityID);
          }
          url = url.replaceAll("#fromDateTime#", fromDateTime);
          url = url.replaceAll("#toDateTime#", toDateTime);
          url = url.replaceAll(
              "#userCompanyId#",
              !widget.isNewCase
                  ? resolutions.userCompanyId.toString()
                  : userData.userModel.userCompanyID.toString());
          url = url.replaceAll("#searchText#", searchController.text.trim());
          print(url);
          await APIViewModel().req<UserDetailsDataAutoSuggAPIModel>(
              context: context,
              url: url,
              reqType: ReqType.Get,
              isLoading: false,
              callback: (model) async {
                if (mounted && model != null) {
                  if (model.success) {
                    listUserDetailsDatasAutoSug.clear();
                    listUserDetailsDatasAutoSug =
                        model.responseData.userDetailsDatas;
                    if (listUserDetailsDatasAutoSug.length > 0) {
                      expandableController.expanded = true;
                    }
                    FocusScope.of(context).requestFocus(new FocusNode());
                    setState(() {
                      isLoading = false;
                    });
                  } else {}
                }
              });
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        } catch (e) {
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          // automaticallyImplyLeading: false,
          elevation: 1,
          backgroundColor: MyTheme.titleColor,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          title: Txt(
              txt: "Submit a new case",
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: true,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    final dateExplast = DateTime(dateNow.year + 50, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);
    return (!widget.isNewCase && (caseTypeMap == null))
        ? SizedBox()
        : Container(
            child: SingleChildScrollView(
              //primary: true,
              child: optCaseType != null
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //case Type
                              Txt(
                                  txt: "Mortgage Type",
                                  txtColor: MyTheme.inputColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                              SizedBox(height: 10),
                              DropDownListDialog(
                                context: context,
                                title: optCaseType.title,
                                ddTitleList: ddCaseType,
                                callback: (optionItem) {
                                  caseIndex = int.parse(optionItem.id);
                                  optCaseType = optionItem;
                                  caseTypeMap = NewCaseCfg()
                                      .getCaseType(optCaseType.title);
                                  optCaseSubType =
                                      OptionItem(id: null, title: "Select");
                                  reloadCaseSubType(int.parse(optCaseType.id));
                                  title.clear();
                                  setState(() {});
                                },
                              ),
                              (ddCaseSubType.listOptionItems.length > 0 &&
                                      optCaseType.id != null)
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 20),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Txt(
                                              txt: "Sub category",
                                              txtColor: MyTheme.inputColor,
                                              txtSize: MyTheme.txtSize,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                          SizedBox(height: 10),
                                          DropDownListDialog(
                                            context: context,
                                            title: optCaseSubType.title,
                                            ddTitleList: ddCaseSubType,
                                            callback: (optionItem) {
                                              optCaseSubType = optionItem;
                                              title.text = optCaseSubType.title;
                                              setState(() {});
                                            },
                                          ),
                                        ],
                                      ),
                                    )
                                  : (isddCaseSubTypeTextField)
                                      ? Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20),
                                          child: drawInputBox(
                                            context: context,
                                            title: "Title",
                                            input: title,
                                            ph: "Please specify",
                                            kbType: TextInputType.text,
                                            inputAction: TextInputAction.next,
                                            focusNode: focusTitle,
                                            focusNodeNext: focusSearch,
                                            len: 50,
                                          ))
                                      : SizedBox(),
                              SizedBox(height: 20),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Txt(
                                        txt: "Applicant/Customer name",
                                        txtColor: MyTheme.inputColor,
                                        txtSize: MyTheme.txtSize - .2,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                  ),
                                  Flexible(
                                    child: GestureDetector(
                                      onTap: () {
                                        Get.to(CreateNewCustomer(
                                          resolutions: widget.resolutions,
                                          isNewCaseApplicant: true,
                                          isNewCase: widget.isNewCase,
                                        )).then((user2) {
                                          user = user2 as UserModel;
                                          if (user != null) {
                                            userDetailsDatas =
                                                UserDetailsDatas.fromJson({
                                              'Id': user.id,
                                              'Name': user.name ?? '',
                                              'Status': 0,
                                              'FirstName': user.firstName,
                                              'LastName': user.lastName,
                                              'Address': user.address ?? '',
                                              'MiddleName':
                                                  user.middleName ?? '',
                                              'UserName': user.userName ?? '',
                                              'Email': user.email ?? '',
                                              'MobileNumber':
                                                  user.mobileNumber ?? '',
                                              'NamePrefix':
                                                  user.namePrefix ?? '',
                                              'LastLoginDate':
                                                  user.lastLoginDate ?? '',
                                              'DateUpdated': '',
                                              'CommunityId': "1",
                                              'CommunityName': '',
                                              'ImageServerUrl': '',
                                              'ThumbnailPath': '',
                                              'ProfileImageUrl':
                                                  user.profileImageURL ?? '',
                                              'UserCompanyId':
                                                  user.userCompanyID ??
                                                      resolutions.userCompanyId,
                                              'CompanyName': '',
                                              'Active': 0,
                                              'LastLoginDateUtc': '',
                                              'LastLoginDateLocal': '',
                                              'CreatedUser': '',
                                              'IsFirstLogin': 0,
                                              'StanfordWorkplaceURL': '',
                                              'GroupName': '',
                                              'PublishDate': '',
                                              'UnreadCount': 0,
                                              'DepartmentName': '',
                                              'BrokerCompanyName': '',
                                            });
                                            listUserDetailsDatasAutoSug
                                                .add(userDetailsDatas);
                                            expandableController.expanded =
                                                true;
                                            setState(() {});
                                          }
                                        });
                                      },
                                      child: Container(
                                        //margin: EdgeInsets.all(20),
                                        //padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            border: Border.all(
                                                width: 1,
                                                color: MyTheme.inputColor)),
                                        child: Icon(
                                          Icons.add_outlined,
                                          color: MyTheme.inputColor,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border(
                                      left: BorderSide(
                                          color: Colors.grey, width: 1),
                                      right: BorderSide(
                                          color: Colors.grey, width: 1),
                                      top: BorderSide(
                                          color: Colors.grey, width: 1),
                                      bottom: BorderSide(
                                          color: Colors.grey, width: 1)),
                                  color: Colors.transparent,
                                ),
                                child: ExpandablePanel(
                                  theme: ExpandableThemeData(
                                      expandIcon: Icons.arrow_drop_down,
                                      collapseIcon: Icons.arrow_drop_up,
                                      iconSize: 30),
                                  controller: expandableController,
                                  collapsed: null,
                                  header: Container(
                                    color: Colors.transparent,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                            flex: 1,
                                            child: Icon(
                                              Icons.search,
                                              color: Colors.grey,
                                              size: 20,
                                            )),
                                        Expanded(
                                          flex: 6,
                                          child: TextField(
                                            controller: searchController,
                                            focusNode: focusSearch,
                                            textInputAction:
                                                TextInputAction.next,
                                            onChanged: (t) async {
                                              streamController.add(t);
                                            },
                                            decoration: new InputDecoration(
                                              border: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              errorBorder: InputBorder.none,
                                              disabledBorder: InputBorder.none,
                                              hintText: "Search using name",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                      FontWeight.normal),
                                              //fillColor: Colors.black,
                                            ),
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 17,
                                            ),
                                            keyboardType: TextInputType.text,
                                          ),
                                        ),
                                        Expanded(
                                            flex: 1,
                                            child: GestureDetector(
                                              onTap: () {
                                                if (widget.isNewCase) {
                                                  user = null;
                                                }
                                                userDetailsDatas = null;
                                                searchController.clear();
                                                FocusScope.of(context)
                                                    .requestFocus(
                                                        new FocusNode());
                                                setState(() {});
                                              },
                                              child:
                                                  searchController.text.length >
                                                          0
                                                      ? Icon(
                                                          Icons.close,
                                                          color: Colors.grey,
                                                          size: 20,
                                                        )
                                                      : SizedBox(),
                                            )),
                                      ],
                                    ),
                                  ),
                                  expanded: Container(
                                    height: getHP(
                                        context,
                                        listUserDetailsDatasAutoSug.length > 0
                                            ? 30
                                            : 0),
                                    //width: getW(context),
                                    child: Scrollbar(
                                      isAlwaysShown:
                                          listUserDetailsDatasAutoSug.length > 0
                                              ? true
                                              : false,
                                      controller: scrollController,
                                      child: ListView(
                                          shrinkWrap: true,
                                          primary: false,
                                          scrollDirection: Axis.vertical,
                                          controller: scrollController,
                                          children: [
                                            ...listUserDetailsDatasAutoSug
                                                .map((model) {
                                              return GestureDetector(
                                                onTap: () {
                                                  expandableController
                                                      .expanded = false;
                                                  userDetailsDatas = model;
                                                  searchController
                                                      .text = userDetailsDatas
                                                          .firstName +
                                                      ' ' +
                                                      userDetailsDatas.lastName;
                                                  setState(() {});
                                                },
                                                child: Card(
                                                  color: Colors.white,
                                                  //elevation: 2,
                                                  child: ListTile(
                                                    leading: CircleAvatar(
                                                      radius: 20,
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      backgroundImage:
                                                          new CachedNetworkImageProvider(
                                                              MyNetworkImage
                                                                  .checkUrl(model
                                                                      .profileImageUrl)),
                                                    ),
                                                    title: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          model.name,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black),
                                                        ),
                                                        SizedBox(height: 5),
                                                        Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Icon(
                                                              Icons.email,
                                                              color:
                                                                  Colors.grey,
                                                              size: 15,
                                                            ),
                                                            Expanded(
                                                              child: Txt(
                                                                  txt: model
                                                                      .email,
                                                                  txtColor: MyTheme
                                                                      .inputColor,
                                                                  txtSize: MyTheme
                                                                          .txtSize -
                                                                      .6,
                                                                  txtAlign:
                                                                      TextAlign
                                                                          .start,
                                                                  isBold:
                                                                      false),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(height: 5),
                                                        Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Icon(
                                                              Icons.phone,
                                                              color:
                                                                  Colors.grey,
                                                              size: 15,
                                                            ),
                                                            Expanded(
                                                              child: Txt(
                                                                  txt: model
                                                                      .mobileNumber,
                                                                  txtColor: MyTheme
                                                                      .inputColor,
                                                                  txtSize: MyTheme
                                                                          .txtSize -
                                                                      .6,
                                                                  txtAlign:
                                                                      TextAlign
                                                                          .start,
                                                                  isBold:
                                                                      false),
                                                            )
                                                          ],
                                                        ),
                                                        /*SizedBox(height: 5),
                                              Container(
                                                color: Colors.white,
                                                height: 2,
                                              )*/
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }).toList(),
                                          ]),
                                    ),
                                  ),
                                ),
                              ),
                              /*ddCaseOwner != null
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 20),
                                      child: DropDownListDialog(
                                        radius: 5,
                                        context: context,
                                        title: optCaseOwner.title,
                                        id: optCaseOwner.id,
                                        ddTitleList: ddCaseOwner,
                                        callback: (optionItem) {
                                          optCaseOwner = optionItem;
                                          setState(() {});
                                        },
                                      ))
                                  : SizedBox(),*/
                              SizedBox(height: 20),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Txt(
                                      txt: "Are you charging a fee?",
                                      txtColor: MyTheme.inputColor,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.center,
                                      isBold: false),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                            child: GestureDetector(
                                                onTap: () {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  isChargingFee = true;
                                                  setState(() {});
                                                },
                                                child: radioButtonitem(
                                                    context: context,
                                                    text: "Yes",
                                                    txtSize: 1.7,
                                                    bgColor: isChargingFee
                                                        ? '#252551'
                                                        : '#FFF',
                                                    textColor: isChargingFee
                                                        ? Colors.white
                                                        : Colors.black))),
                                        SizedBox(width: 10),
                                        Flexible(
                                            child: GestureDetector(
                                                onTap: () {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  isChargingFee = false;
                                                  setState(() {});
                                                },
                                                child: radioButtonitem(
                                                    context: context,
                                                    text: "No",
                                                    txtSize: 1.7,
                                                    bgColor: !isChargingFee
                                                        ? '#252551'
                                                        : '#FFF',
                                                    textColor: !isChargingFee
                                                        ? Colors.white
                                                        : Colors.black))),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              drawChargingFeeBox(
                                  chargingFee,
                                  focusChargingFee,
                                  ddChargingFee,
                                  optChargingFee,
                                  isChargingFee,
                                  isChargingFeeRefundable, (opt) {
                                optChargingFee.title = opt.title;
                                setState(() {});
                              }, (isRef) {
                                isChargingFeeRefundable = isRef;
                                setState(() {});
                              }),
                              SizedBox(height: 20),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Txt(
                                      txt: "Are you charging another fee?",
                                      txtColor: MyTheme.inputColor,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.center,
                                      isBold: false),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                            child: GestureDetector(
                                                onTap: () {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  isChargingAnotherFee = true;
                                                  setState(() {});
                                                },
                                                child: radioButtonitem(
                                                    context: context,
                                                    text: "Yes",
                                                    txtSize: 1.7,
                                                    bgColor:
                                                        isChargingAnotherFee
                                                            ? '#252551'
                                                            : '#FFF',
                                                    textColor:
                                                        isChargingAnotherFee
                                                            ? Colors.white
                                                            : Colors.black))),
                                        SizedBox(width: 10),
                                        Flexible(
                                            child: GestureDetector(
                                                onTap: () {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  isChargingAnotherFee = false;
                                                  setState(() {});
                                                },
                                                child: radioButtonitem(
                                                    context: context,
                                                    text: "No",
                                                    txtSize: 1.7,
                                                    bgColor:
                                                        !isChargingAnotherFee
                                                            ? '#252551'
                                                            : '#FFF',
                                                    textColor:
                                                        !isChargingAnotherFee
                                                            ? Colors.white
                                                            : Colors.black))),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              drawChargingFeeBox(
                                  chargingAnotherFee,
                                  focusChargingAnotherFee,
                                  ddChargingAnotherFee,
                                  optChargingAnotherFee,
                                  isChargingAnotherFee,
                                  isChargingAnotherFeeRefundable, (opt) {
                                optChargingAnotherFee.title = opt.title;
                                setState(() {});
                              }, (isRef) {
                                isChargingAnotherFeeRefundable = isRef;
                                setState(() {});
                              }),
                              SizedBox(height: 20),
                              Txt(
                                  txt: "Product Type",
                                  txtColor: MyTheme.inputColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        //hide keyboard
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          isProductTypeRegular = true;
                                        });
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "Regular",
                                          bgColor: isProductTypeRegular
                                              ? '#252551'
                                              : '#FFF',
                                          txtSize: 1.5,
                                          textColor: isProductTypeRegular
                                              ? Colors.white
                                              : Colors.black),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        //hide keyboard
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          isProductTypeRegular = false;
                                        });
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "Product Transfer",
                                          txtSize: 1.5,
                                          bgColor: isProductTypeRegular
                                              ? '#FFF'
                                              : '#252551',
                                          textColor: isProductTypeRegular
                                              ? Colors.black
                                              : Colors.white),
                                    ),
                                  )
                                ],
                              ),

                              // product type end
                              //SPV Start

                              caseTypeMap.length > 0 &&
                                      caseTypeMap['isSPV'] as bool
                                  ? Column(
                                      children: [
                                        SizedBox(height: 20),
                                        Txt(
                                            txt:
                                                "Are you buying the property in name of a SPV (Limited Company)",
                                            txtColor: MyTheme.inputColor,
                                            txtSize: MyTheme.txtSize - .2,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                        SizedBox(height: 10),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: GestureDetector(
                                                onTap: () {
                                                  //hide keyboard
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  setState(() {
                                                    isSPVCompany = true;
                                                  });
                                                },
                                                child: radioButtonitem(
                                                    context: context,
                                                    text: "Yes",
                                                    bgColor: isSPVCompany
                                                        ? '#252551'
                                                        : '#FFF',
                                                    textColor: isSPVCompany
                                                        ? Colors.white
                                                        : Colors.black),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: GestureDetector(
                                                onTap: () {
                                                  //hide keyboard
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  setState(() {
                                                    isSPVCompany = false;
                                                  });
                                                },
                                                child: radioButtonitem(
                                                    context: context,
                                                    text: "No",
                                                    bgColor: isSPVCompany
                                                        ? '#FFF'
                                                        : '#252551',
                                                    textColor: isSPVCompany
                                                        ? Colors.black
                                                        : Colors.white),
                                              ),
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        isSPVCompany
                                            ? Container(
                                                child: Column(
                                                  children: [
                                                    inputFieldWithText(
                                                        context: context,
                                                        controller:
                                                            companyNameController,
                                                        titleText:
                                                            "Name of Company",
                                                        hintText:
                                                            "Name of Company"),
                                                    inputFieldWithText(
                                                        context: context,
                                                        controller:
                                                            registerAddressController,
                                                        titleText:
                                                            "Registered Address",
                                                        hintText:
                                                            "Registered Address"),
                                                    inputFieldWithText(
                                                        context: context,
                                                        controller:
                                                            registerNumberController,
                                                        titleText:
                                                            "Company registration number",
                                                        hintText:
                                                            "Registration number"),
                                                    SizedBox(height: 15),
                                                    DatePickerView(
                                                      txtColor:
                                                          MyTheme.inputColor,
                                                      cap: 'Date registered',
                                                      dt: (regDate == '')
                                                          ? 'DD-MM-YYYY'
                                                          : regDate,
                                                      initialDate: dateExpfirst,
                                                      firstDate: dateExpfirst,
                                                      lastDate: dateExplast,
                                                      callback: (value) {
                                                        if (mounted) {
                                                          setState(() {
                                                            try {
                                                              regDate = DateFormat(
                                                                      'dd-MM-yyyy')
                                                                  .format(value)
                                                                  .toString();
                                                            } catch (e) {
                                                              myLog(
                                                                  e.toString());
                                                            }
                                                          });
                                                        }
                                                      },
                                                    ),
                                                    SizedBox(height: 20),
                                                    Align(
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      child: Txt(
                                                          txt:
                                                              "Is the company a trading company?",
                                                          txtColor: MyTheme
                                                              .inputColor,
                                                          txtSize:
                                                              MyTheme.txtSize -
                                                                  .2,
                                                          txtAlign:
                                                              TextAlign.start,
                                                          isBold: false),
                                                    ),
                                                    SizedBox(height: 10),
                                                    Row(
                                                      children: [
                                                        Expanded(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () {
                                                              //hide keyboard
                                                              FocusScope.of(
                                                                      context)
                                                                  .requestFocus(
                                                                      FocusNode());
                                                              setState(() {
                                                                isTradingCompany =
                                                                    true;
                                                              });
                                                            },
                                                            child: radioButtonitem2(
                                                                context:
                                                                    context,
                                                                text: "Yes",
                                                                bgColor: isTradingCompany
                                                                    ? MyTheme
                                                                        .purpleColor
                                                                    : Color(
                                                                        0xFF000),
                                                                textColor:
                                                                    isTradingCompany
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        Expanded(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () {
                                                              //hide keyboard
                                                              FocusScope.of(
                                                                      context)
                                                                  .requestFocus(
                                                                      FocusNode());
                                                              setState(() {
                                                                isTradingCompany =
                                                                    false;
                                                              });
                                                            },
                                                            child: radioButtonitem2(
                                                                context:
                                                                    context,
                                                                text: "No",
                                                                bgColor: isTradingCompany
                                                                    ? MyTheme
                                                                        .purpleColor
                                                                    : Color(
                                                                        0xFF000),
                                                                textColor:
                                                                    !isTradingCompany
                                                                        ? Colors
                                                                            .white
                                                                        : Colors
                                                                            .black),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : SizedBox(),
                                      ],
                                    )
                                  : SizedBox(),

                              //SPV End
                              SizedBox(height: 20),
                              drawCaseClassification(),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Txt(
                                      txt:
                                          "Please confirm that you have your client's authority to use their personal information for this application.",
                                      txtColor: MyTheme.inputColor,
                                      txtSize: MyTheme.txtSize - .3,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            //hide keyboard
                                            FocusScope.of(context)
                                                .requestFocus(FocusNode());
                                            setState(() {
                                              isClientAuthority = true;
                                            });
                                          },
                                          child: radioButtonitem2(
                                              context: context,
                                              text: "Yes",
                                              bgColor: isClientAuthority
                                                  ? MyTheme.purpleColor
                                                  : Color(0xFF000),
                                              textColor: isTradingCompany
                                                  ? Colors.white
                                                  : Colors.black),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            //hide keyboard
                                            FocusScope.of(context)
                                                .requestFocus(FocusNode());
                                            setState(() {
                                              isClientAuthority = false;
                                            });
                                          },
                                          child: radioButtonitem2(
                                              context: context,
                                              text: "No",
                                              bgColor: !isClientAuthority
                                                  ? MyTheme.purpleColor
                                                  : Color(0xFF000),
                                              textColor: !isTradingCompany
                                                  ? Colors.white
                                                  : Colors.black),
                                        ),
                                      )
                                    ],
                                  ),
                                  isClientAuthority
                                      ? Padding(
                                          padding:
                                              const EdgeInsets.only(top: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Txt(
                                                      txt: "Disclosure method",
                                                      txtColor:
                                                          MyTheme.inputColor,
                                                      txtSize:
                                                          MyTheme.txtSize - .4,
                                                      txtAlign: TextAlign.start,
                                                      isBold: false),
                                                  SizedBox(height: 2),
                                                  DropDownListDialog(
                                                    context: context,
                                                    title: optDisclosureMethod
                                                        .title,
                                                    ddTitleList:
                                                        ddDisclosureMethod,
                                                    vPadding: 2,
                                                    txtSize:
                                                        (MyTheme.txtSize - .4),
                                                    callback: (optionItem) {
                                                      optDisclosureMethod =
                                                          optionItem;
                                                      setState(() {});
                                                    },
                                                  ),
                                                ],
                                              ),
                                              SizedBox(height: 10),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Txt(
                                                      txt: "Advice type",
                                                      txtColor:
                                                          MyTheme.inputColor,
                                                      txtSize:
                                                          MyTheme.txtSize - .4,
                                                      txtAlign: TextAlign.start,
                                                      isBold: false),
                                                  SizedBox(height: 2),
                                                  DropDownListDialog(
                                                    context: context,
                                                    title: optAdviceType.title,
                                                    ddTitleList: ddAdviceType,
                                                    vPadding: 2,
                                                    txtSize:
                                                        (MyTheme.txtSize - .4),
                                                    callback: (optionItem) {
                                                      optAdviceType =
                                                          optionItem;
                                                      setState(() {});
                                                    },
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        )
                                      : SizedBox(),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: MMBtn(
                              txt: "Save & Continue",
                              //txtColor: Colors.white,
                              //bgColor: HexColor.fromHex("#3351A5"),
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                              radius: 5,
                              callback: () {
                                if (optCaseType.id == null) {
                                  showToast(
                                      context: context,
                                      msg:
                                          "Please select 'Lead Type' from the above options");
                                  return;
                                }
                                if (optCaseType.title != 'Others') {
                                  if (ddCaseSubType.listOptionItems.length >
                                          0 &&
                                      optCaseSubType.id == null) {
                                    showToast(
                                        context: context,
                                        msg: "Please choose sub category");
                                    return;
                                  }
                                } else {
                                  if (UserProfileVal().isEmpty(context, title,
                                      "Please enter lead title")) {
                                    return;
                                  }
                                }

                                if (userDetailsDatas == null) {
                                  showToast(
                                    context: context,
                                    msg:
                                        "Please select Applicant/Customer from selection option",
                                  );
                                  return;
                                }

                                if (isClientAuthority) {
                                  if (optDisclosureMethod.id == null) {
                                    showToast(
                                      context: context,
                                      msg: "Please select Disclosure method",
                                    );
                                    return;
                                  }
                                  if (optAdviceType.id == null) {
                                    showToast(
                                      context: context,
                                      msg: "Please select advice type",
                                    );
                                    return;
                                  }
                                }

                                final param = {
                                  "Status": 903,
                                  "Title": caseTypeMap['title'] ?? '',
                                  "Description": title.text.trim() ?? '',
                                  "IsInPersonOrOnline": false,
                                  "DutDateType": 0,
                                  "DeliveryDate": DateTime.now().toString(),
                                  "DeliveryTime": "",
                                  "WorkerNumber": 1,
                                  "Skill": "",
                                  "IsFixedPrice": true,
                                  "HourlyRate": 0,
                                  "FixedBudgetAmount": 0,
                                  "NetTotalAmount": 0,
                                  "JobCategory": isProductTypeRegular
                                      ? "Regular"
                                      : "Product Transfer",
                                  "PreferedLocation": "",
                                  "Requirements": "",
                                  "TotalHours": 0,
                                  "Latitude": 0,
                                  "Longitude": 0,
                                  "TaskReferenceNumber": "",
                                  "ReferenceTaskerId": userData.userModel.id,
                                  "UserQuickSourceId": 0,
                                  "MortgageCaseInfoEntityModelList": [],
                                  "AreYouChargingAFee":
                                      isChargingFee ? 'Yes' : 'No',
                                  "ChargeFeeAmount":
                                      chargingFee.text.trim().isNotEmpty
                                          ? int.parse(chargingFee.text.trim())
                                          : 0,
                                  "ChargingFeeWhenPayable":
                                      optChargingFee.title,
                                  "ChargingFeeRefundable":
                                      isChargingFeeRefundable ? 'Yes' : 'No',
                                  "AreYouChargingAnotherFee":
                                      isChargingAnotherFee ? 'Yes' : 'No',
                                  "ChargeAnotherFeeAmount":
                                      chargingAnotherFee.text.trim().isNotEmpty
                                          ? int.parse(
                                              chargingAnotherFee.text.trim())
                                          : 0,
                                  "ChargingAnotherFeeWhenPayable":
                                      optChargingAnotherFee.title,
                                  "ChargingAnotherFeeRefundable":
                                      isChargingAnotherFeeRefundable
                                          ? 'Yes'
                                          : 'No',
                                  "AdviceType": isClientAuthority
                                      ? optAdviceType.title
                                      : '',
                                  "DisclosureMethod": isClientAuthority
                                      ? optDisclosureMethod.title
                                      : '',
                                  "CaseClassification":
                                      caseClassificationIndex.toString(),
                                  "UserId": widget.isNewCase
                                      ? userData.userModel.id
                                      : resolutions.userId,
                                  "EntityId":
                                      widget.isNewCase ? 0 : resolutions.id,
                                  "EntityName": widget.isNewCase
                                      ? ''
                                      : resolutions.userNoteEntityName ??
                                          'Lead',
                                  "ReferenceAdviserId": userData.userModel.id,
                                  "CaseCreationDeclarationSelection": "Yes",
                                  "CompanyId": userData.userModel.userCompanyID,
                                  "MortgageCaseInfoEntity": {
                                    "UserId": widget.isNewCase
                                        ? userData.userModel.id
                                        : resolutions.userId,
                                    "CompanyId": 0,
                                    "TaskId": 0,
                                    "Status": 0,
                                    "CreationDate": DateTime.now().toString(),
                                    "UpdatedDate": DateTime.now().toString(),
                                    "VersionNumber": 0,
                                    "CaseType": "",
                                    "CustomerType": "",
                                    "IsSmoker": "No",
                                    "Remarks": "",
                                    "IsAnyOthers": "No",
                                    "IsCurrentProperty": "No",
                                    "CustomerName": userDetailsDatas != null
                                        ? (userDetailsDatas.firstName +
                                                ' ' +
                                                userDetailsDatas.lastName) ??
                                            ''
                                        : '',
                                    "CustomerEmail": userDetailsDatas != null
                                        ? userDetailsDatas.email ?? ''
                                        : '',
                                    "CustomerMobileNumber": userDetailsDatas !=
                                            null
                                        ? userDetailsDatas.mobileNumber ?? ''
                                        : '',
                                    "CustomerAddress": "",
                                    "CoapplicantUserId": 0,
                                    "AreYouBuyingThePropertyInNameOfASPV":
                                        isSPVCompany ? "Yes" : "No",
                                    "CompanyName":
                                        companyNameController.text.trim(),
                                    "RegisteredAddress":
                                        registerAddressController.text.trim(),
                                    "DateRegistered": regDate,
                                    "CompanyRegistrationNumber":
                                        registerNumberController.text.trim(),
                                    "DateRegistered1": "",
                                    "DateRegistered2": "",
                                    "DateRegistered3": "",
                                    "AdminFee": 0,
                                    "AdminFeeWhenPayable": "",
                                    "AdviceFee": 0,
                                    "AdviceFeeWhenPayable": "",
                                    "IsFeesRefundable": "",
                                    "FeesRefundable": "",
                                    "IsTheCompanyATradingCompany":
                                        isTradingCompany ? "Yes" : "No",
                                  }
                                };
                                myLog(json.encode(param));
                                PostCaseAPIMgr().wsOnPostCase2(
                                  context: context,
                                  param: param,
                                  callback: (model) async {
                                    if (model != null && mounted) {
                                      try {
                                        if (model.success) {
                                          final LocationsModel caseModel =
                                              model.responseData.task;
                                          //  email notification api call
                                          await APIViewModel()
                                              .req<CommonAPIModel>(
                                            context: context,
                                            url: ServerIntr
                                                .SENDCASEEMAILNOTIFY4BSBROKER_URL
                                                .replaceAll("#caseId#",
                                                    caseModel.id.toString()),
                                            isLoading: true,
                                            reqType: ReqType.Get,
                                          );
                                          Get.to(
                                            () => WebScreen(
                                              caseID: caseModel.id.toString(),
                                              title: caseTypeMap['title'],
                                              url: CaseDetailsWebHelper()
                                                  .getLink(
                                                title: caseTypeMap['title'],
                                                taskId: caseModel.id,
                                              ),
                                            ),
                                          ).then((value) {
                                            if (widget.isNewCase) {
                                              resetData();
                                            } else
                                              Get.back();
                                          });
                                        } else {
                                          showAlert(
                                              context: context,
                                              msg: "Failed to create case");
                                        }
                                      } catch (e) {
                                        myLog(e.toString());
                                      }
                                    }
                                  },
                                );
                              }),
                        ),
                        SizedBox(height: 30),
                      ],
                    )
                  : SizedBox(),
            ),
          );
  }

  getHowMuchBox(
      {TextEditingController controller, String titleText, bool showLongTxt}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Txt(
              txt: titleText,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          showLongTxt
              ? Txt(
                  txt:
                      "If you're not sure, your best guess is fine at this point.",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true)
              : SizedBox(),
          SizedBox(height: 10),
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.white),
            child: Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Txt(
                        txt: "£",
                        txtColor: Colors.grey,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true)),
                Expanded(
                  flex: 6,
                  child: new TextFormField(
                    controller: controller,
                    maxLength: 10,
                    decoration: new InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      hintText: "0",
                      counterText: "",
                      hintStyle: TextStyle(color: Colors.grey),
                      fillColor: Colors.black,
                    ),
                    validator: (val) {
                      if (val.length == 0) {
                        return "Name cannot be empty";
                      } else {
                        return null;
                      }
                    },
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  drawCaseClassification() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
            txt: "Case classification",
            txtColor: MyTheme.inputColor,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.start,
            isBold: false,
          ),
          SizedBox(height: 5),
          drawRadioGroup(
              type: eRadioType.VERTICAL,
              isMainBG: false,
              map: listCaseClassification,
              selected: caseClassificationIndex,
              callback: (v) {
                caseClassificationIndex = v;
                setState(() {});
              })
          /*Theme(
              data: MyTheme.radioThemeData,
              child: RadioListTile(
                dense: true,
                contentPadding: EdgeInsets.zero,
                value: 0,
                groupValue: caseClassificationIndex,
                onChanged: (v) {
                  caseClassificationIndex = v;
                  setState(() {});
                },
                title: Text("Advised case (Introducer advice)",
                    style: TextStyle(color: Colors.black, fontSize: 15)),
              )),
          Theme(
              data: MyTheme.radioThemeData,
              child: RadioListTile(
                dense: true,
                contentPadding: EdgeInsets.zero,
                value: 1,
                groupValue: caseClassificationIndex,
                onChanged: (v) {
                  caseClassificationIndex = v;
                  setState(() {});
                },
                title: Text("Non advised case (Brightstar Advice)",
                    style: TextStyle(color: Colors.black, fontSize: 15)),
              )),*/
        ],
      ),
    );
  }
}
