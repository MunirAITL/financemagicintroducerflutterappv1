import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class DDList extends StatefulWidget {
  final Map<int, dynamic> list;
  final String hint;
  final bool readyOnly;
  final String btnText;
  final Function(int key, String value) onItemChange;
  final Function onClicked;
  const DDList({
    Key key,
    @required this.list,
    @required this.hint,
    this.readyOnly = true,
    this.btnText,
    @required this.onItemChange,
    this.onClicked,
  }) : super(key: key);
  @override
  _DDFieldState createState() => _DDFieldState();
}

class _DDFieldState extends State<DDList> with Mixin {
  final FocusNode _focusNode = FocusNode();
  OverlayEntry _overlayEntry;
  final LayerLink _layerLink = LayerLink();

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        this._overlayEntry = this._createOverlayEntry();
        Overlay.of(context).insert(this._overlayEntry);
      } else {
        try {
          this._overlayEntry.remove();
          this._overlayEntry = null;
        } catch (e) {}
      }
    });
  }

  OverlayEntry _createOverlayEntry() {
    RenderBox renderBox = context.findRenderObject();
    var size = renderBox.size;
    return OverlayEntry(
        builder: (context) => Positioned(
              width: size.width,
              child: CompositedTransformFollower(
                link: this._layerLink,
                showWhenUnlinked: false,
                offset: Offset(0.0, size.height + 0.0),
                child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  color: MyTheme.bgColor2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Scrollbar(
                        isAlwaysShown: true,
                        child: Container(
                          height: widget.list.length > 5
                              ? getHP(context, 20)
                              : null,
                          child: ListView(shrinkWrap: true, children: [
                            ...widget.list.entries.map((e) => InkWell(
                                  onTap: () {
                                    widget.onItemChange(e.key, e.value);
                                  },
                                  child: Container(
                                    width: getW(context),
                                    color: Colors.transparent,
                                    child: Material(
                                        elevation: 1,
                                        color: MyTheme.bgColor2,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10,
                                              right: 5,
                                              top: 5,
                                              bottom: 5),
                                          child: Txt(
                                              txt: e.value,
                                              txtColor: Colors.black,
                                              txtSize: 1.6,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                        )),
                                  ),
                                )),
                          ]),
                        ),
                      ),
                      (widget.btnText != null)
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: OutlinedButton(
                                onPressed: () {
                                  this._overlayEntry.remove();
                                  this._overlayEntry = null;
                                  _focusNode.removeListener(() {});
                                  if (widget.onClicked != null)
                                    widget.onClicked();
                                },
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: MyTheme.greyColor,
                                  side: BorderSide(
                                      width: .5, color: Colors.black),
                                ),
                                child: const Text(
                                  "Search",
                                  style: TextStyle(color: Colors.black),
                                ),
                              ))
                          : SizedBox()
                    ],
                  ),
                ),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
      link: this._layerLink,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: MyTheme.statusBarColor),
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 3, bottom: 3),
          child: TextFormField(
            focusNode: this._focusNode,
            readOnly: widget.readyOnly,
            style: TextStyle(
              color: Colors.black,
              fontSize: 12,
            ),
            textAlign: TextAlign.center,
            decoration: new InputDecoration(
                isDense: true,
                counterText: '',
                hintText: widget.hint,
                hintStyle: new TextStyle(color: Colors.black87, fontSize: 12),
                labelStyle: new TextStyle(color: Colors.black, fontSize: 12),
                contentPadding: EdgeInsets.only(top: 3, left: 5, right: 5),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                suffixIconConstraints:
                    BoxConstraints(minHeight: 20, minWidth: 20),
                suffixIcon: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: Icon(Icons.arrow_drop_down,
                      color: Colors.black, size: 20),
                )),
          ),
        ),
      ),
    );
  }
}
