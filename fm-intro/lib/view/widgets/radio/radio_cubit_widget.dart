import 'package:aitl/view_model/rx/cubit/radio_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

BlocProvider<dynamic> drawRadioCubitWidget(
    {@required List<String> list, bool isRadio}) {
  return BlocProvider(
      create: (_) => RadioCubit(),
      child: BlocBuilder<RadioCubit, bool>(
          builder: (context, v) => Column(
                children: [
                  ...list.map(
                    (m) => Row(children: [
                      Container(
                          child: IconButton(
                        onPressed: () {
                          print(v);
                          context.read<RadioCubit>().onChanged(isRadio);
                        },
                        icon: Icon(Icons.check_box_outlined),
                      )),
                      Text(m, style: TextStyle(color: Colors.black))
                    ]),
                  )
                ],
              )));
}
