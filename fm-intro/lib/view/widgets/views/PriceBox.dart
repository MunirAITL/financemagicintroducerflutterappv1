import 'package:aitl/config/AppDefine.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

getCurSign() => AppDefine.CUR_SIGN;
drawPrice({
  String curSign,
  String price = "0.0",
  double txtSize = 20.0,
  bool isBold = false,
  Color txtColor = Colors.black,
}) {
  if (curSign == null) curSign = getCurSign();
  //return SvgPicture.asset('assets/images/svg/cur/ico_pound2x.svg');
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        curSign,
        style: TextStyle(
          color: txtColor,
          fontSize: txtSize,
          fontWeight: (isBold) ? FontWeight.bold : FontWeight.normal,
        ),
      ),
      SizedBox(width: 5),
      Text(
        price,
        style: TextStyle(
          color: txtColor,
          fontSize: txtSize,
          fontWeight: (isBold) ? FontWeight.bold : FontWeight.normal,
        ),
      ),
    ],
  );
}
