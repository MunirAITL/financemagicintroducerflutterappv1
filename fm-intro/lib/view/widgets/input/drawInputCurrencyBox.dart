import 'package:aitl/view/widgets/input/utils/DecimalTextInputFormatter.dart';
import 'package:flutter/material.dart';

import '../../../config/AppDefine.dart';
import '../../../config/MyTheme.dart';
import '../txt/Txt.dart';

drawInputCurrencyBox({
  BuildContext context,
  TextEditingController tf,
  double hintTxt = 0,
  String labelTxt,
  int len,
  FocusNode focusNode,
  FocusNode focusNodeNext,
  String sign = AppDefine.CUR_SIGN,
  Function(String) onChange,
}) {
  return Container(
    //color: Colors.amber,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        labelTxt != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: labelTxt,
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            : SizedBox(),
        Container(
          child: Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      bottomLeft: Radius.circular(5)),
                  color: Colors.grey.shade400,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Txt(
                      txt: sign,
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize + .3,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
              Expanded(
                child: TextField(
                  textInputAction: TextInputAction.next,
                  controller: tf,
                  focusNode: focusNode,
                  inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  maxLength: len,
                  onChanged: (v) {
                    if (onChange != null) onChange(v);
                  },
                  onEditingComplete: () {
                    // Move the focus to the next node explicitly.
                    if (focusNode != null) {
                      focusNode.unfocus();
                    } else {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    }
                    if (focusNodeNext != null) {
                      FocusScope.of(context).requestFocus(focusNodeNext);
                    } else {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    }
                  },
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    height: MyTheme.txtLineSpace,
                  ),
                  decoration: new InputDecoration(
                    counterText: "",
                    hintText: hintTxt.toStringAsFixed(0),
                    hintStyle: new TextStyle(
                      color: Colors.grey,
                      fontSize: 17,
                      height: MyTheme.txtLineSpace,
                    ),
                    contentPadding: EdgeInsets.only(left: 20, right: 20),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      borderSide: BorderSide(width: 1, color: Colors.black),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10)),
                      borderSide: BorderSide(width: 1, color: Colors.grey),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        borderSide: BorderSide(
                          width: 1,
                        )),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
