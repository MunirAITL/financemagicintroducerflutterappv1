import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/mixin.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';

InputBoxMM4({
  context,
  ctrl,
  lableTxt,
  kbType,
  len,
  minLines,
  maxLines,
  isPwd = false,
  autofocus = false,
  prefixIco,
  suffixIco,
  txtAlign = TextAlign.start,
  txtSize,
  onChange,
  isReadonly = false,
}) {
  if (txtSize == null) {
    txtSize = MyTheme.txtSize;
  }
  //double width = MediaQuery.of(context).size.width;
  //double height = MediaQuery.of(context).size.height;
  return Container(
    //color: Colors.black,
    child: TextFormField(
      readOnly: isReadonly,
      controller: ctrl,
      autofocus: autofocus,
      keyboardType: kbType,
      minLines: minLines,
      maxLines: maxLines,
      /*onChanged: (widget.ecap != eCap.None)
            ? (string) => (subject.add(string))
            : widget.onChange,*/
      onChanged: onChange,
      //textCapitalization: TextCapitalization.sentences,
      //textCapitalization: TextCapitalization.sentences,
      /*inputFormatters: (widget.kbType == TextInputType.phone)
            ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
            : (widget.kbType == TextInputType.emailAddress)
                ? [
                    FilteringTextInputFormatter.allow(RegExp(
                        "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                  ]
                : null,*/
      inputFormatters: (kbType == TextInputType.phone)
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : (kbType == TextInputType.emailAddress)
              ? [
                  FilteringTextInputFormatter.allow(RegExp(
                      "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                ]
              : null,
      obscureText: isPwd,
      maxLength: len,
      autocorrect: false,
      enableSuggestions: false,
      textAlign: txtAlign,
      style: TextStyle(
        color: Colors.black,
        fontSize: 17,
        height: MyTheme.txtLineSpace,
      ),
      decoration: new InputDecoration(
        isDense: true,
        counter: Offstage(),
        //contentPadding: const EdgeInsets.symmetric(vertical: 20.0),
        prefixIcon: (prefixIco != null)
            ? Container(
                child: Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Container(
                    decoration: BoxDecoration(
                      color: MyTheme.dBlueAirColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                      ),
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 13, bottom: 0),
                      child: prefixIco,
                    ),
                  ),
                ),
              )
            : null,
        suffixIcon: (suffixIco != null)
            ? Container(
                decoration: BoxDecoration(
                  color: MyTheme.dBlueAirColor,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, top: 13, right: 10),
                  child: suffixIco,
                ),
              )
            : null,
        counterText: "",
        //filled: true,
        //fillColor: Colors.white,
        hintText: lableTxt,
        hintStyle: new TextStyle(
          color: Colors.grey,
          fontSize: 17,
          height: MyTheme.txtLineSpace,
        ),
        labelStyle: new TextStyle(
          color: Colors.black,
          fontSize: 17,
          height: MyTheme.txtLineSpace,
        ),
        contentPadding: EdgeInsets.only(
          left: (prefixIco != null) ? 0 : 10,
          right: (prefixIco != null) ? 50 : 10,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: const BorderRadius.all(
            const Radius.circular(10.0),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 1),
          borderRadius: const BorderRadius.all(
            const Radius.circular(10.0),
          ),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 1),
          borderRadius: const BorderRadius.all(
            const Radius.circular(10.0),
          ),
        ),
      ),
    ),
  );
}
