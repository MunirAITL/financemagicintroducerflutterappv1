import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

class Btn extends StatelessWidget {
  final String txt;
  final Color txtColor;
  final double txtSize;
  final Color bgColor;
  final double width;
  final double height;
  final double radius;
  final Function callback;

  Btn({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.bgColor,
    @required this.width,
    @required this.height,
    this.radius = 10,
    this.txtSize,
    @required this.callback,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      //color: Colors.brown,
      child: MaterialButton(
        child: Txt(
          txt: txt,
          txtColor: txtColor,
          txtSize: (this.txtSize == null) ? MyTheme.txtSize : this.txtSize,
          txtAlign: TextAlign.center,
          isBold: true,
        ),
        onPressed: () => callback(),
        //textColor: Colors.black,
        color: bgColor,
        shape: OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                width: 1.0,
                color: Colors.transparent),
            borderRadius: new BorderRadius.circular(radius)),
      ),
    );
  }
}
