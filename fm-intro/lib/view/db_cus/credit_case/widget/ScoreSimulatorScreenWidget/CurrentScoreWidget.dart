import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetSimulatedScoreUseridAPIModel.dart';
import 'package:aitl/view/db_cus/credit_case/Controller/ScoreHelper.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class CurrentStoreWidget extends StatelessWidget with Mixin {
  final CRUserManage cRUserManage;
  final SimulatedScore simulatedScore;
  final String title;
  final String scenerioTxt;
  final double radius;
  final bool isTxtSmall;
  const CurrentStoreWidget({
    Key key,
    this.cRUserManage,
    this.simulatedScore,
    @required this.title,
    @required this.scenerioTxt,
    @required this.radius,
    @required this.isTxtSmall,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var score = 0;
    if (cRUserManage != null) {
      score = cRUserManage.score;
    } else if (simulatedScore != null) {
      score = simulatedScore.simulatedScore;
    }

    return Flexible(
      child: Container(
        //color: Colors.black,
        child: Column(
          children: [
            Container(
              child: Txt(
                  txt: title,
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - ((isTxtSmall) ? 0.2 : 0),
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: getW(context),
              child: CircularPercentIndicator(
                radius: radius,
                animation: true,
                animationDuration: 1200,
                lineWidth: 15.0,
                percent: (score / 710) /*.toPrecision(2)*/,
                center: Txt(
                    txt: score.toString(),
                    txtColor: Colors.green,
                    txtSize: MyTheme.txtSize + ((isTxtSmall) ? 1 : 2),
                    txtAlign: TextAlign.center,
                    isBold: false),
                circularStrokeCap: CircularStrokeCap.butt,
                backgroundColor: Colors.grey,
                progressColor: ScoreHelper().getCircleBarColor(score),
              ),
            ),
            SizedBox(height: 20),
            Container(
              child: Txt(
                  txt: ScoreHelper().getCircleStatus(score),
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - ((isTxtSmall) ? 0.2 : 0),
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            SizedBox(height: 10),
            Container(
              child: Txt(
                  txt: scenerioTxt,
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - ((isTxtSmall) ? 0.2 : 0),
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
          ],
        ),
      ),
    );
  }
}
