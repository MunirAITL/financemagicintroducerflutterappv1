import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/credit_case/AlertDialog/AlertDialogAccountAndAlerts.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AccountAndAlerts extends StatelessWidget with Mixin {
  const AccountAndAlerts({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getW(context),
      child: Card(
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  showToolTips(
                      context: context,
                      w: Column(
                        children: [
                          Txt(
                              txt: "Account and Alerts",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          Txt(
                              txt: "The following information is displayed:",
                              txtColor: Colors.black54,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          Txt(
                              txt: "The number of days to the next report",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 10),
                          Txt(
                              txt:
                                  "The expiry date of the account information about credit alerts, total number and any unread alerts",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ],
                      ));
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.warning,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Txt(
                        txt: "Account and Alerts",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true),
                    SizedBox(width: 10),
                    Icon(
                      Icons.info_outlined,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Container(
                width: getW(context),
                child: Padding(
                  padding: const EdgeInsets.only(left: 28.0),
                  child: Txt(
                      txt: "25 days until your next credit report",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
              SizedBox(height: 10),
              Container(
                width: getW(context),
                child: Padding(
                  padding: const EdgeInsets.only(left: 28.0),
                  child: Txt(
                      txt: "Account Expires on: 12 Mar 2022",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
