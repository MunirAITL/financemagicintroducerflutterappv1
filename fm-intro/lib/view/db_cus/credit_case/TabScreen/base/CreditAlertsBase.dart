import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import '../../../../../Mixin.dart';

abstract class CreditAlertsBase<T extends StatefulWidget> extends State<T>
    with Mixin {
  drawAbout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Txt(
                  txt: "Credit Alerts",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(height: 20),
              Txt(
                  txt:
                      "Alerts indicating important changes to your credit information is displayed on this page.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "You will be emailed when there is a new alert.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "There are no alerts.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w600,
                  isBold: true),
            ],
          ),
        ),
      ),
    );
  }
}
