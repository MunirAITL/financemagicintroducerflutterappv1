import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/utils/tokbox_cfg.dart';
import 'package:aitl/controller/api/db_cus/my_cases/MyCasesAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/timeline/GroupChatAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/timeline/TimelineAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/view/db_cus/timeline/tokbox/video_call_page.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'TimeLineBase.dart';
import 'package:get/get.dart';

class TimeLineTab extends StatefulWidget {
  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends BaseTimeline<TimeLineTab>
    with StateListener, TickerProviderStateMixin {
  static const platform = const MethodChannel('flutter.native/tokbox');

  StateProvider _stateProvider;
  TabController tabController;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar3_reload_case_api) {
      getRefreshDataChat();
    }
  }

  onPageLoadChat() async {
    try {
      setState(() {
        isLoading = true;
      });
      TimelineAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        status: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> timelineUserModel =
                      model.responseData.users;
                  if (timelineUserModel != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (timelineUserModel.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (TimelineUserModel user in timelineUserModel) {
                        listTimelineUserModelChat.add(user);
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }

                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(context: context, msg: "Timelines not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            myLog("Time line tab screen not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  onPageLoadGroup() async {
    try {
      setState(() {
        isLoading = true;
      });
      GroupChatAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        caseStatus: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                listCaseGroupMessageData =
                    model.responseData.caseGroupMessageData;
                setState(() {
                  isLoading = false;
                });
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    isLoading = false;
                    showToast(context: context, msg: "Cases not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            myLog("my case tab new not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> getRefreshDataChat() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTimelineUserModelChat.clear();
    onPageLoadChat();
  }

  Future<void> getRefreshDataGroup() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listCaseGroupMessageData.clear();
    onPageLoadGroup();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    tabController.dispose();
    tabController = null;
    listTimelineUserModelChat = null;
    listCaseGroupMessageData = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
      } else {
        if (tabController.index == 0) {
          print("0");
          getRefreshDataChat();
        } else if (tabController.index == 1) {
          print("1");
          getRefreshDataGroup();
        }
      }
    });
    try {
      getRefreshDataChat();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Messages"),
          /*actions: [
            Server.isTest
                ? GestureDetector(
                    onTap: () async {
                      //OpenTokConfig().set(
                      //"T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9ZDc5ODI0MGM4NmM2MjE4Y2ZjZGIwMTE2OGJhZmZmNzdhNGVlOWNkNzpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzMzcxNTkxJm5vbmNlPTAuMjI5MjAzMTk1MDkwNzkyOSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNjQ1OTYzNTkzJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9");
                      OpenTokConfig().set((userData.userModel.email ==
                              "munir@aitl.net") //?publisher : subscriber
                          ? "T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9NWMwNTAwMGVhNjFjMDliNjY0ODExYmVjYmU5NjljZGE3MTA0M2ZiZDpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzODAyMTA1Jm5vbmNlPTAuMzc4NTI4MDIwNzA3NTQ2NzYmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTY0NjM5NDEwNCZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ=="
                          : "T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9ZGY2NTZiOWEyODFmZDljMGYwMjFlMjNlYjUzNDhlOTgzMWVkNzg4YzpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzODAyMTUwJm5vbmNlPTAuOTM2NzgyNDI1MTYxNzQzOCZyb2xlPXN1YnNjcmliZXImZXhwaXJlX3RpbWU9MTY0NjM5NDE0OSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==");

                      if (Platform.isIOS) {
                        // iOS-specific code
                        final param = {
                          'apiKey': OpenTokConfig.API_KEY,
                          'sessionId': OpenTokConfig.SESSION_ID,
                          'token': OpenTokConfig.TOKEN
                        };
                        await platform.invokeMethod('start', param);
                      } else {
                        Get.to(() => CallWidget());
                      }
                    },
                    child: Container(
                      width: getWP(context, 25),
                      color: Colors.transparent,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Flexible(
                                child: Text("Support",
                                    style: TextStyle(color: Colors.white))),
                            Flexible(
                                child: Icon(Icons.video_call,
                                    color: Color(0xFF66FF00), size: 30)),
                          ],
                        ),
                      ),
                    ))
                : SizedBox(),
            SizedBox(width: 5)
          ],*/
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(getHP(context, 7)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                (isLoading)
                    ? AppbarBotProgBar(
                        backgroundColor: MyTheme.appbarProgColor,
                      )
                    : SizedBox(),
                Container(
                  color: MyTheme.bgColor2,
                  height: getHP(context, 7),
                  child: DecoratedBox(
                    //This is responsible for the background of the tabbar, does the magic
                    decoration: BoxDecoration(
                      //This is for background color
                      color: Colors.white.withOpacity(0.0),
                      //This is for bottom border that is needed
                      border: Border(
                          bottom: BorderSide(color: Colors.black, width: 1.4)),
                    ),
                    child: TabBar(
                      controller: tabController,
                      //labelColor: Colors.deepOrange,
                      unselectedLabelColor: Colors.black45,
                      labelColor: Colors.black,
                      indicatorColor: Color(0xFF00A368),
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicatorWeight: 3,
                      tabs: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.chat_outlined,
                                color: MyTheme.titleColor, size: 22),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                txt: "Chat",
                                txtColor: MyTheme.titleColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                fontWeight: FontWeight.w500,
                                isBold: false,
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.group_rounded,
                                color: MyTheme.titleColor),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                txt: "Group",
                                txtColor: MyTheme.titleColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                fontWeight: FontWeight.w500,
                                isBold: false,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: TabBarView(
            controller: tabController,
            children: [
              drawChatView(),
              drawGroupChatView(),
            ],
          ),
        ),
      ),
    );
  }
}
