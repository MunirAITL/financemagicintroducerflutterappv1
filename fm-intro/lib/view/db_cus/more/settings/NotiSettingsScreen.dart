import 'dart:ui';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/more/settings/NotiSettingsCfg.dart';
import 'package:aitl/controller/api/db_cus/more/settings/NotiSettingsAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/more/settings/TestNofiAPIMgr.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

class NotiSettingsScreen extends StatefulWidget {
  @override
  State createState() => _NotiSettingsScreenState();
}

class _NotiSettingsScreenState extends State<NotiSettingsScreen> with Mixin {
  NotiSettingsCfg notiSettingsCfg;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    notiSettingsCfg = null;
    super.dispose();
  }

  appInit() async {
    try {
      notiSettingsCfg = NotiSettingsCfg();
      NotiSettingsAPIMgr().wsGetNotificationSettingsAPI(
        context: context,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final caseUpdateMap = notiSettingsCfg.listOpt[0];
                  final caseRecomendationMap = notiSettingsCfg.listOpt[1];
                  final caseCompletedMap = notiSettingsCfg.listOpt[2];
                  final caseReminderMap = notiSettingsCfg.listOpt[3];
                  final userUpdateMap = notiSettingsCfg.listOpt[4];
                  final helpfulInfoMap = notiSettingsCfg.listOpt[5];
                  final updateNewsLetterMap = notiSettingsCfg.listOpt[6];

                  //  caseUpdate
                  caseUpdateMap['isEmail'] = model
                      .responseData.userNotificationSetting.isCaseUpdateEmail;
                  caseUpdateMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseUpdateSMS;
                  caseUpdateMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseUpdateNotification;
                  //  caseRecomendation
                  caseRecomendationMap['isEmail'] = model.responseData
                      .userNotificationSetting.isCaseRecomendationEmail;
                  caseRecomendationMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseRecomendationNotification;
                  //  caseCompleted
                  caseCompletedMap['isEmail'] = model.responseData
                      .userNotificationSetting.isCaseCompletedEmail;
                  caseCompletedMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseCompletedSMS;
                  caseCompletedMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseCompletedNotification;
                  //  caseReminder
                  caseReminderMap['isEmail'] = model
                      .responseData.userNotificationSetting.isCaseReminderEmail;
                  caseReminderMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseReminderSMS;
                  caseReminderMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseReminderNotification;
                  //  userUpdateMap
                  userUpdateMap['isEmail'] = model
                      .responseData.userNotificationSetting.isUserUpdateEmail;
                  userUpdateMap['isPush'] = model.responseData
                      .userNotificationSetting.isUserUpdateNotification;
                  //  helpfulInfoMap
                  helpfulInfoMap['isEmail'] = model.responseData
                      .userNotificationSetting.isHelpfulInformationEmail;
                  helpfulInfoMap['isPush'] = model.responseData
                      .userNotificationSetting.isHelpfulInformationNotification;
                  //  updateNewsLetterMap
                  updateNewsLetterMap['isEmail'] = model.responseData
                      .userNotificationSetting.isUpdateAndNewsLetterEmail;
                  updateNewsLetterMap['isPush'] = model
                      .responseData
                      .userNotificationSetting
                      .isUpdateAndNewsLetterNotification;

                  //notiSettingsCfg.listOpt[6] = updateNewsLetterMap;
                  //myLog(notiSettingsCfg.listOpt[6].toString());

                  setState(() {});
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(
                        context: context,
                        msg: "Notifications Settings not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        },
      );
    } catch (e) {}
    try {
      /*await NotiSettingsHelper().getSettings().then((listOpt) {
        if (listOpt != null) {
          notiSettingsCfg.listOpt = listOpt;
          setState(() {});
        } else {}
      });*/
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Notification settings"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getW(context),
      height: getH(context),
      child: (notiSettingsCfg == null)
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    Txt(
                        txt:
                            "Your notifications can be updated\nat any time via options below",
                        txtColor: MyTheme.titleColor,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.left,
                        fontWeight: FontWeight.w500,
                        txtLineSpace: 1.4,
                        isBold: true),
                    SizedBox(height: 20),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Txt(
                              txt: "is it working ?".capitalizeFirstofEach,
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          BtnOutline(
                            txt: "Test it",
                            txtColor: Colors.black,
                            borderColor: MyTheme.titleColor,
                            callback: () async {
                              // showNotificationMessage("Finance Magic says hi!", "If you\'re seeing this, it means everything\'s OK!");

                              TestNotiAPIMgr().wsTestNotiAPI(
                                context: context,
                                callback: (model) {
                                  if (model != null && mounted) {
                                    try {
                                      if (model.success) {
                                        try {
                                          final msg = model.responseData
                                              .notification.description;
                                          showToast(
                                              context: context,
                                              msg: msg,
                                              which: 1);
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      } else {
                                        try {
                                          final err = model
                                              .messages.pushMessage[0]
                                              .toString();
                                          showToast(
                                              context: context,
                                              msg: err,
                                              which: 0);
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      }
                                    } catch (e) {
                                      myLog(e.toString());
                                    }
                                  } else {
                                    myLog("Notification screen not in");
                                  }
                                },
                              );
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          // SizedBox(height: 10),
                          Expanded(
                            child: Txt(
                                txt:
                                    "make sure you're actually getting those all important push notifications",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                //txtLineSpace: 1.2,
                                maxLines: 10,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w400,
                                isBold: false),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(color: Colors.black, height: .5),
                    SizedBox(height: 10),
                    for (var listOpt in notiSettingsCfg.listOpt)
                      Padding(
                        padding: const EdgeInsets.only(top: 10, bottom: 10),
                        child: Container(
                          width: double.infinity,
                          //color: Colors.black,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: listOpt['title']
                                      .toString()
                                      .capitalizeFirstofEach,
                                  txtColor: MyTheme.titleColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                              SizedBox(height: 5),
                              Txt(
                                  txt: listOpt['subTitle'],
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  //txtLineSpace: 1.2,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                              SizedBox(height: 10),
                              (listOpt['email'] as bool)
                                  ? Theme(
                                      data: MyTheme.radioThemeData,
                                      child: Transform.translate(
                                        offset: Offset(-10, 0),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Transform.scale(
                                              scale: 1.2,
                                              child: Checkbox(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                value: listOpt['isEmail'],
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    listOpt['isEmail'] =
                                                        newValue;
                                                  });
                                                },
                                              ),
                                            ),
                                            SizedBox(width: 10),
                                            Txt(
                                                txt: 'Email',
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                              (listOpt['sms'] as bool)
                                  ? Theme(
                                      data: MyTheme.radioThemeData,
                                      child: Transform.translate(
                                        offset: Offset(-10, 0),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Transform.scale(
                                              scale: 1.2,
                                              child: Checkbox(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                value: listOpt['isSms'],
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    listOpt['isSms'] = newValue;
                                                  });
                                                },
                                              ),
                                            ),
                                            SizedBox(width: 10),
                                            Txt(
                                                txt: 'Sms',
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                              (listOpt['push'] as bool)
                                  ? Theme(
                                      data: MyTheme.radioThemeData,
                                      child: Transform.translate(
                                        offset: Offset(-10, 0),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Transform.scale(
                                              scale: 1.2,
                                              child: Checkbox(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                value: listOpt['isPush'],
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    listOpt['isPush'] =
                                                        newValue;
                                                  });
                                                },
                                              ),
                                            ),
                                            SizedBox(width: 10),
                                            Txt(
                                                txt: 'Push',
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                      ),
                    SizedBox(height: 20),
                    MMBtn(
                      txt: "Save",
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 5,
                      callback: () async {
                        NotiSettingsAPIMgr().wsPostNotificationSettingsAPI(
                          context: context,
                          notiSettingsCfg: notiSettingsCfg,
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  try {
                                    final msg = model
                                        .messages.postNotificationSetting[0]
                                        .toString();
                                    showToast(
                                        context: context, msg: msg, which: 1);
                                    //await NotiSettingsHelper().setSettings(notiSettingsCfg.listOpt);
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                } else {
                                  try {
                                    if (mounted) {
                                      final err = model
                                          .messages.postNotificationSetting[0]
                                          .toString();
                                      showToast(
                                          context: context, msg: err, which: 1);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          },
                        );
                      },
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            ),
    );
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) {}
  static Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future<void> showNotificationMessage(String title, String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.max, priority: Priority.high, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, title, body, platformChannelSpecifics,
        payload: 'Default_Sound');
  }
}
