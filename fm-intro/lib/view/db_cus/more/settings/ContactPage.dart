import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/ContactModel.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ContactPage extends StatefulWidget {
  const ContactPage({Key key}) : super(key: key);

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  List<ContactModel> list;

  @override
  void initState() {
    super.initState();

    list = [
      ContactModel(
          head: "A", url: "", name: "Alex Ventura", mobile: "+1 334 344444444"),
      ContactModel(
          head: "A", url: "", name: "AVentura M", mobile: "+1 334 344444444"),
      ContactModel(
          head: "A",
          url: "",
          name: "A. Sardar Bhai",
          mobile: "+1 334 344444444"),
      ContactModel(
          head: "A", url: "", name: "A. Designer", mobile: "+1 334 344444444"),
    ];
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.statusBarColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          title: UIHelper().drawAppbarTitle(title: "Contact"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onPanDown: (detail) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: drawLayout()),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            SizedBox(height: 20),
            Container(
              child: TextField(
                maxLength: 50,
                autocorrect: false,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                  height: MyTheme.txtLineSpace,
                ),
                decoration: InputDecoration(
                  isDense: true,
                  filled: true,
                  counterText: '',
                  hintStyle: TextStyle(color: Colors.grey[800]),
                  hintText: "",
                  fillColor: Colors.white,
                  contentPadding: EdgeInsets.only(left: 20, right: 20),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                    size: 30,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(20),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width: 1),
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(20),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 1),
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(20),
                    ),
                  ),
                ),
              ),
            ),
            drawItem(head: "A", listContactModel: list),
          ],
        ),
      ),
    );
  }

  drawItem({String head = "", List<ContactModel> listContactModel}) {
    return Column(
      children: [
        SizedBox(height: 10),
        Txt(
          txt: head,
          txtColor: Colors.white,
          txtSize: MyTheme.txtSize + .2,
          txtAlign: TextAlign.center,
          isBold: true,
        ),
        SizedBox(height: 10),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            children: [
              for (var model in listContactModel)
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: CircleAvatar(
                              radius: 20,
                              backgroundColor: Colors.transparent,
                              backgroundImage: new CachedNetworkImageProvider(
                                MyNetworkImage.checkUrl((model != null)
                                    ? model.url
                                    : Server.MISSING_IMG),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 6,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Txt(
                                  txt: model.name,
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  isBold: true,
                                  txtAlign: TextAlign.start,
                                  maxLines: 1,
                                ),
                                SizedBox(height: 10),
                                Txt(
                                  txt: model.mobile,
                                  txtColor: Colors.grey,
                                  txtSize: MyTheme.txtSize - .4,
                                  isBold: false,
                                  txtAlign: TextAlign.start,
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                              child: Icon(Icons.arrow_forward_ios,
                                  color: Colors.grey, size: 15))
                        ],
                      ),
                    ),
                  ],
                )
            ],
          ),
        ),
      ],
    );
  }
}
