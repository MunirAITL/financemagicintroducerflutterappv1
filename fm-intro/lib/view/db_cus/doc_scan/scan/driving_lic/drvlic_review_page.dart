import 'dart:convert';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/DocScanCfg.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/driving_lic/drvlic_result_page.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mime_type/mime_type.dart';
import 'drvlic_review_base.dart';

class DrvLicReviewPage extends StatefulWidget {
  const DrvLicReviewPage({Key key}) : super(key: key);
  @override
  State createState() => _DrvLicReviewPageState();
}

class _DrvLicReviewPageState extends DrvLicReviewBase<DrvLicReviewPage> {
  final list = [
    "Check the following:",
    "* Image is sharp",
    "* There are no reflections",
    "* All personal details are visible"
  ];

  @override
  void initState() {
    animationController = new AnimationController(
        duration: new Duration(seconds: 1), vsync: this);
    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animateScanAnimation(true);
      } else if (status == AnimationStatus.dismissed) {
        animateScanAnimation(false);
      }
    });
    animateScanAnimation(false);
    setState(() {
      isFrontAnim = false;
    });

    super.initState();
  }

  @override
  void dispose() {
    if (animationController != null) animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          title: UIHelper().drawAppbarTitle(title: "Prove your identity"),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 30,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: getHP(context, 70),
              child: Stack(
                children: [
                  Container(
                    decoration: new BoxDecoration(
                        color: MyTheme.statusBarColor,
                        borderRadius: new BorderRadius.only(
                          bottomLeft: const Radius.circular(40),
                          bottomRight: const Radius.circular(40),
                        )),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 30),
                      child: drawPicBox(),
                    ),
                  ),
                  Positioned(
                    top: getHP(context, 30),
                    left: 40,
                    right: 40,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/doc_scan/eid_provide_id_box.png"),
                          fit: BoxFit.cover,
                        ),
                        //color: Colors.white,
                        borderRadius: new BorderRadius.only(
                          bottomLeft: const Radius.circular(10),
                          bottomRight: const Radius.circular(10),
                        ),
                        //border: Border.all(color: Colors.grey, width: 1)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: [
                            UIHelper().drawReviewBox(
                                context: context,
                                title: "Review photo of driving licence"),
                            drawIssues(list),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: (!isFrontAnim)
                  ? MMBtn(
                      txt: "Confirm",
                      width: getW(context),
                      height: getHP(context, 7),
                      radius: 5,
                      callback: () {
                        wsUploadDoc();
                      })
                  : SizedBox(),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10, left: 20, right: 20, bottom: 10),
              child: (!isFrontAnim)
                  ? MMBtn(
                      txt: "Rescan",
                      width: getW(context),
                      height: getHP(context, 7),
                      bgColor: Color(0xFFDFE2E7),
                      txtColor: Color(0xFF263F5D),
                      radius: 5,
                      callback: () async {
                        Get.off(() => OpenCamDrvLicPage());
                      })
                  : SizedBox(),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  wsUploadDoc() async {
    String mimeType = mime(scanDocData.file_drvlic_front.path);
    var fileName = (scanDocData.file_drvlic_front.path.split('/').last);
    //String mimee = mimeType.split('/')[0];
    String type = mimeType.split('/')[1];
    List<int> fileInByte = scanDocData.file_drvlic_front.readAsBytesSync();
    final imageData = base64Encode(fileInByte);
    //print(imageData);
    final param = {
      "UserCompanyId": userData.userModel.userCompanyID,
      "ImageType": type,
      "FileName": fileName,
      "UserId": userData.userModel.id,
      "ContentData": imageData,
      "DocumentType": "1",
      "RequestId": scanDocData.requestId,
    };
    //final jsonString = JsonString(json.encode(param));
    //myLog(jsonString.source);
    await APIViewModel().req<PostDocByBase64BitAPIModel>(
        context: context,
        url: DocScanCfg.DOC_POST_URL,
        reqType: ReqType.Post,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
        },
        param: param,
        callback: (model2) async {
          if (mounted) {
            if (model2 != null) {
              if (model2.success) {
                if (model2.responseData.responsedata.organizeOcrData == null) {
                  tryAgainAlert(
                      isDocVerify: false,
                      msg:
                          'Sorry, image quality is too lower. Please re-upload document image.');
                } else {
                  wsUploadDocVerify();
                }
              } else {
                tryAgainAlert(isDocVerify: false);
              }
            } else {
              tryAgainAlert(isDocVerify: false);
            }
          }
        });
  }

  wsUploadDocVerify() async {
    await APIViewModel().req<PostDocVerifyAPIModel>(
        context: context,
        url: DocScanCfg.VERIFY_POST_URL,
        reqType: ReqType.Post,
        param: {
          "Requestid": scanDocData.requestId,
        },
        callback: (model3) {
          if (mounted) {
            if (model3 != null) {
              if (model3.success) {
                Get.off(() => DrvLicResultPage(model: model3));
              } else {
                tryAgainAlert(isDocVerify: true);
              }
            } else {
              tryAgainAlert(isDocVerify: true);
            }
          }
        });
  }

  tryAgainAlert(
      {bool isDocVerify, String msg = 'Something went wrong. Try again?'}) {
    confirmDialog(
      context: context,
      title: "Uploading Alert",
      msg: msg,
      callbackYes: () {
        if (!isDocVerify)
          wsUploadDoc();
        else
          wsUploadDocVerify();
      },
    );
  }
}
