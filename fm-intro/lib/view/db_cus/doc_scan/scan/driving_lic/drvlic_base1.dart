import 'package:flutter/material.dart';

import '../../../../../Mixin.dart';

abstract class DrvLic1Base<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();
}
