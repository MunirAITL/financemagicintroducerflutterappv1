import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'drvlic_base2.dart';

class DrvLicPage2 extends StatefulWidget {
  const DrvLicPage2({Key key}) : super(key: key);
  @override
  State createState() => _DrvLicPage2State();
}

class _DrvLicPage2State extends DrvLic2Base<DrvLicPage2> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Scan identity document"),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 30),
            Container(
              width: getWP(context, 40),
              height: getHP(context, 12),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/doc_scan/driving_lic.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt:
                      "Is your driving licence English and does it has a transparent circular window you can see through?",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            drawRow(),
          ],
        ),
      ),
    );
  }
}
