import 'package:aitl/config/MyTheme.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../Mixin.dart';
import 'pp_open_cam_base.dart';

class OpenCamPPPage extends StatefulWidget {
  const OpenCamPPPage({Key key}) : super(key: key);
  @override
  State createState() {
    return _OpenCamPPPageState();
  }
}

class _OpenCamPPPageState extends OpenCamPPBase<OpenCamPPPage>
    with WidgetsBindingObserver, TickerProviderStateMixin, Mixin {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
  }

  loadCam() async {
    final cameras = await availableCameras();
    controller = CameraController(cameras[0], ResolutionPreset.veryHigh);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    loadCam();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          key: scaffoldKey,
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor2,
          /*/appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor2,
          title: Txt(
              txt: "Smart card completed",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize - .5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),*/
          body: drawLayout()),
    );
  }

  drawLayout() {
    if (controller == null) return drawEmpty();
    return controller.value.isInitialized
        ? Stack(children: [
            Positioned.fill(
                child: AspectRatio(
                    aspectRatio: controller.value.aspectRatio,
                    child: CameraPreview(controller))),
            new Positioned.fill(
              child: new Image.asset(
                'assets/images/doc_scan/cam_card_bg.png',
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
                top: getHP(context, 1),
                right: 5,
                child: Container(
                  //width: getW(context),
                  child: IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.close,
                        color: MyTheme.brandColor,
                        size: 30,
                      )),
                )),
            Positioned(
              top: getHP(context, 20),
              child: Container(
                width: getW(context),
                child: Center(
                  child: Text(
                    "Scan your passport",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Positioned(
              top: (getH(context) / 3),
              left: 40,
              child: Container(
                width: getW(context) - 80,
                height: getHP(context, 27),
                child: Image.asset(
                  "assets/images/doc_scan/eid_cam_rect.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              top: getHP(context, 75),
              left: getW(context) / 2 - 30,
              child: /*(isFront)
                    ?*/
                  GestureDetector(
                onTap: () {
                  onTakePictureButtonPressed();
                },
                child: Container(
                  width: 60,
                  height: 60,
                  child: Image.asset("assets/images/ico/cam_ico.png"),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: MyTheme.brandColor,
                  ),
                ),
              ),
              /*: Container(
                        width: getW(context),
                        child: IconButton(
                            iconSize: 40,
                            onPressed: onTakePictureButtonPressed,
                            icon: Icon(
                              Icons.refresh,
                              color: Colors.white,
                            ))),*/
            ),
          ])
        : drawEmpty();
  }

  drawEmpty() {
    return Center(child: CircularProgressIndicator());
  }
}
