import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/base_card.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/selfie/selfie_base.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/selfie/selfie_open_cam_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/selfie/uploading_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl_pkg/classes/ImageLib.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../Mixin.dart';

class SelfiePage extends StatefulWidget {
  const SelfiePage({Key key}) : super(key: key);
  @override
  State createState() => _SelfiePageState();
}

class _SelfiePageState extends SelfieBase<SelfiePage> {
  @override
  void initState() {
    super.initState();
    scanDocData.file_selfie = null;
    scanDocData.file_drvlic_front = null;
    scanDocData.file_pp_front = null;
  }

  @override
  void dispose() {
    super.dispose();
    scanDocData.file_selfie = null;
    scanDocData.file_drvlic_front = null;
    scanDocData.file_pp_front = null;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Take a Selfie"),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    //print(userData.userModel.userCompanyID);
    return Container(
      width: getW(context),
      height: getH(context),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 1),
            Container(
              width: getW(context),
              height: getHP(context, 55),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/doc_scan/selfie_bg.png'),
                  fit: BoxFit.cover,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
            Center(
              child: Txt(
                  txt: "Take a Selfie",
                  txtColor: HexColor.fromHex("#C51733"),
                  txtSize: MyTheme.txtSize + .5,
                  txtAlign: TextAlign.center,
                  style: TextStyle(
                    color: HexColor.fromHex("#C51733"),
                    shadows: [
                      Shadow(
                        color: HexColor.fromHex("#C51733"),
                        offset: Offset(0, 0),
                        blurRadius: 0,
                      ),
                      Shadow(
                        color: Colors.grey,
                        offset: Offset(1.5, 1.5),
                        blurRadius: 2,
                      ),
                    ],
                    fontWeight: FontWeight.w500,
                    fontSize: 22,
                  ),
                  isBold: true),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Txt(
                  txt:
                      "To prove that it is really you on the identity document, you need to take a photo of yourself, a selfie. Make sure your face is clearly visible and you're the only person in the photo.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 20, right: 20, bottom: 20),
                child: MMBtn(
                    txt: "Continue",
                    width: getW(context),
                    height: getHP(context, 7),
                    radius: 5,
                    callback: () async {
                      if (Server.isOtp) {
                        Get.off(() => OpenCamSelfiePage());
                      } else {
                        scanDocData.file_selfie =
                            await ImageLib.getImageFromAssets(
                                "eid_scan/" + BaseCard.SELFIE);
                        Get.off(() => UploadingPage());
                      }
                    })),
          ],
        ),
      ),
    );
  }
}
