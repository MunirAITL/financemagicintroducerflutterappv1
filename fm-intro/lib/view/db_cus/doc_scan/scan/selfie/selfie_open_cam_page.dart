import 'package:aitl/config/MyTheme.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import '../../../../../Mixin.dart';
import 'selfie_open_cam_base.dart';

class OpenCamSelfiePage extends StatefulWidget {
  const OpenCamSelfiePage({Key key}) : super(key: key);
  @override
  State createState() {
    return _OpenCamSelfiePageState();
  }
}

class _OpenCamSelfiePageState extends OpenCamSelfieBase<OpenCamSelfiePage>
    with WidgetsBindingObserver, TickerProviderStateMixin, Mixin {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
  }

  Future<CameraDescription> _getCamera(CameraLensDirection dir) async {
    return await availableCameras().then(
      (List<CameraDescription> cameras) => cameras.firstWhere(
        (CameraDescription camera) => camera.lensDirection == dir,
      ),
    );
  }

  loadCam() async {
    //final cameras = await availableCameras();
    if (controller != null) {
      controller.dispose();
      controller = null;
    }
    controller = CameraController(
        await _getCamera(direction), ResolutionPreset.veryHigh);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    loadCam();
  }

  @override
  void dispose() {
    controller.dispose();
    controller = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        /*/appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor2,
          title: Txt(
              txt: "Smart card completed",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize - .5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),*/
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    if (controller == null) return drawEmpty();
    return controller.value.isInitialized
        ? Stack(children: [
            Positioned.fill(
                child: AspectRatio(
                    aspectRatio: controller.value.aspectRatio,
                    child: CameraPreview(controller))),
            new Positioned.fill(
              child: new Image.asset(
                'assets/images/doc_scan/cam_face_bg.png',
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
              top: (getH(context) / 3.5),
              left: 50,
              child: Container(
                width: getW(context) - 100,
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/doc_scan/eid_cam_rect.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              top: getHP(context, 5),
              right: 20,
              child: Container(
                //width: getW(context),
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: MyTheme.brandColor),
                child: IconButton(
                    onPressed: () {
                      isFront = !isFront;
                      if (isFront)
                        direction = CameraLensDirection.front;
                      else
                        direction = CameraLensDirection.back;
                      loadCam();
                      //setState(() {});
                    },
                    icon: Icon(
                      (isFront)
                          ? Icons.photo_camera_front
                          : Icons.photo_camera_back,
                      color: Colors.white,
                      size: 25,
                    )),
              ),
            ),
            Positioned(
                top: getHP(context, 20),
                child: Container(
                  width: getW(context),
                  child: Center(
                    child: Text(
                      "Take a selfie",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )),
            Positioned(
                top: getHP(context, 75),
                child: /*(isFront)
                    ?*/
                    Container(
                  width: getW(context),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: MyTheme.brandColor),
                  child: Center(
                    child: IconButton(
                        iconSize: 25,
                        icon: const Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                        ),
                        color: Colors.blue,
                        onPressed: onTakePictureButtonPressed),
                  ),
                )
                /*: Container(
                        width: getW(context),
                        child: IconButton(
                            iconSize: 40,
                            onPressed: onTakePictureButtonPressed,
                            icon: Icon(
                              Icons.refresh,
                              color: Colors.white,
                            ))),*/
                ),
          ])
        : drawEmpty();
  }

  drawEmpty() {
    return Container(
      width: getW(context),
      height: getH(context),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/doc_scan/selfie_bg.png"),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
