import 'PostEmailOtpAPIModel.dart';

class SendUserEmailOtpAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  SendUserEmailOtpAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  SendUserEmailOtpAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> getSenduseremailotplogin;
  Messages({this.getSenduseremailotplogin});
  Messages.fromJson(Map<String, dynamic> json) {
    getSenduseremailotplogin = json['get_senduseremailotplogin'].cast<String>();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['get_senduseremailotplogin'] = this.getSenduseremailotplogin;
    return data;
  }
}

class ResponseData {
  UserOTP userOTP;

  ResponseData({this.userOTP});

  ResponseData.fromJson(Map<String, dynamic> json) {
    userOTP =
        json['UserOTP'] != null ? new UserOTP.fromJson(json['UserOTP']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userOTP != null) {
      data['UserOTP'] = this.userOTP.toJson();
    }
    return data;
  }
}
