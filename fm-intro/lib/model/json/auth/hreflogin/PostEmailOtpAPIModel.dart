class PostEmailOtpAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  PostEmailOtpAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostEmailOtpAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> postUserotp;
  Messages({this.postUserotp});

  Messages.fromJson(Map<String, dynamic> json) {
    postUserotp = json['post_userotp'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_userotp'] = this.postUserotp;
    return data;
  }
}

class ResponseData {
  UserOTP userOTP;

  ResponseData({this.userOTP});

  ResponseData.fromJson(Map<String, dynamic> json) {
    userOTP =
        json['UserOTP'] != null ? new UserOTP.fromJson(json['UserOTP']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userOTP != null) {
      data['UserOTP'] = this.userOTP.toJson();
    }
    return data;
  }
}

class UserOTP {
  String mobileNumber;
  String email;
  int status;
  int id;

  UserOTP({this.mobileNumber, this.status, this.id});

  UserOTP.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'] ?? '';
    email = json['Email'] ?? '';
    status = json['Status'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Email'] = this.email;
    data['Status'] = this.status;
    data['Id'] = this.id;
    return data;
  }
}
