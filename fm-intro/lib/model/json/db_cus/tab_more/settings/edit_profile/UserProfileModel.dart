class UserProfileModel {
  String firstName;
  String lastName;
  String name;
  String email;
  String userName;
  String profileImageUrl;
  int profileImageId;
  String coverImageUrl;
  int coverImageId;
  String stanfordWorkplaceURL;
  String headline;
  String briefBio;
  String referenceId;
  String referenceType;
  String remarks;
  String cohort;
  String communityId;
  bool isFirstLogin;
  String mobileNumber;
  String dateofBirth;
  String userProfileUrl;
  String address;
  String latitude;
  String longitude;
  String workHistory;
  int userTaskCategoryId;
  String status;
  String middleName;
  String namePrefix;
  String areYouASmoker;
  String countryCode;
  String addressLine1;
  String addressLine2;
  String addressLine3;
  String town;
  String county;
  String postcode;
  String telNumber;
  String nationalInsuranceNumber;
  String nationality;
  String countryofBirth;
  String countryofResidency;
  String passportNumber;
  String maritalStatus;
  String occupantType;
  String livingDate;
  String password;
  int userCompanyId;
  String visaExpiryDate;
  String passportExpiryDate;
  String visaName;
  String otherVisaName;
  int id;

  UserProfileModel({
    this.firstName,
    this.lastName,
    this.name,
    this.email,
    this.userName,
    this.profileImageUrl,
    this.profileImageId,
    this.coverImageUrl,
    this.coverImageId,
    this.stanfordWorkplaceURL,
    this.headline,
    this.briefBio,
    this.referenceId,
    this.referenceType,
    this.remarks,
    this.cohort,
    this.communityId,
    this.isFirstLogin,
    this.mobileNumber,
    this.dateofBirth,
    this.userProfileUrl,
    this.address,
    this.latitude,
    this.longitude,
    this.workHistory,
    this.userTaskCategoryId,
    this.status,
    this.middleName,
    this.namePrefix,
    this.areYouASmoker,
    this.countryCode,
    this.addressLine1,
    this.addressLine2,
    this.addressLine3,
    this.town,
    this.county,
    this.postcode,
    this.telNumber,
    this.nationalInsuranceNumber,
    this.nationality,
    this.countryofBirth,
    this.countryofResidency,
    this.passportNumber,
    this.maritalStatus,
    this.occupantType,
    this.livingDate,
    this.password,
    this.userCompanyId,
    this.visaExpiryDate,
    this.passportExpiryDate,
    this.visaName,
    this.otherVisaName,
    this.id,
  });

  factory UserProfileModel.fromJson(Map<String, dynamic> j) {
    return UserProfileModel(
      firstName: j['FirstName'] ?? '',
      lastName: j['LastName'] ?? '',
      name: j['Name'] ?? '',
      email: j['Email'] ?? '',
      userName: j['UserName'] ?? '',
      profileImageUrl: j['ProfileImageUrl'] ?? '',
      profileImageId: j['ProfileImageId'] ?? 0,
      coverImageUrl: j['CoverImageUrl'] ?? '',
      coverImageId: j['CoverImageId'] ?? 0,
      stanfordWorkplaceURL: j['StanfordWorkplaceURL'] ?? '',
      headline: j['Headline'] ?? '',
      briefBio: j['BriefBio'] ?? '',
      referenceId: j['ReferenceId'] ?? '',
      referenceType: j['ReferenceType'] ?? '',
      remarks: j['Remarks'] ?? '',
      cohort: j['Cohort'] ?? '',
      communityId: j['CommunityId'] ?? '',
      isFirstLogin: j['IsFirstLogin'] ?? false,
      mobileNumber: j['MobileNumber'] ?? '',
      dateofBirth: j['DateofBirth'] ?? '',
      userProfileUrl: j['UserProfileUrl'] ?? '',
      address: j['Address'] ?? '',
      latitude: j['Latitude'] ?? '',
      longitude: j['Longitude'] ?? '',
      workHistory: j['WorkHistory'] ?? '',
      userTaskCategoryId: j['UserTaskCategoryId'] ?? 0,
      status: j['Status'] ?? '',
      middleName: j['MiddleName'] ?? '',
      namePrefix: j['NamePrefix'] ?? '',
      areYouASmoker: j['AreYouASmoker'] ?? '',
      countryCode: j['CountryCode'] ?? '',
      addressLine1: j['AddressLine1'] ?? '',
      addressLine2: j['AddressLine2'] ?? '',
      addressLine3: j['AddressLine3'] ?? '',
      town: j['Town'] ?? '',
      county: j['County'] ?? '',
      postcode: j['Postcode'] ?? '',
      telNumber: j['TelNumber'] ?? '',
      nationalInsuranceNumber: j['NationalInsuranceNumber'] ?? '',
      nationality: j['Nationality'] ?? '',
      countryofBirth: j['CountryofBirth'] ?? '',
      countryofResidency: j['CountryofResidency'] ?? '',
      passportNumber: j['PassportNumber'] ?? '',
      maritalStatus: j['MaritalStatus'] ?? '',
      occupantType: j['OccupantType'] ?? '',
      livingDate: j['LivingDate'] ?? '',
      password: j['Password'] ?? '',
      userCompanyId: j['UserCompanyId'] ?? 0,
      visaExpiryDate: j['VisaExpiryDate'] ?? '',
      passportExpiryDate: j['PassportExpiryDate'] ?? '',
      visaName: j['VisaName'] ?? '',
      otherVisaName: j['OtherVisaName'] ?? '',
      id: j['Id'] ?? 0,
    );
  }

  Map<String, dynamic> toMap() => {
        'FirstName': firstName,
        'LastName': lastName,
        'Name': name,
        'Email': email,
        'UserName': userName,
        'ProfileImageUrl': profileImageUrl,
        'ProfileImageId': profileImageId,
        'CoverImageUrl': coverImageUrl,
        'CoverImageId': coverImageId,
        'StanfordWorkplaceURL': stanfordWorkplaceURL,
        'Headline': headline,
        'BriefBio': briefBio,
        'ReferenceId': referenceId,
        'ReferenceType': referenceType,
        'Remarks': remarks,
        'Cohort': cohort,
        'CommunityId': communityId,
        'IsFirstLogin': isFirstLogin,
        'MobileNumber': mobileNumber,
        'DateofBirth': dateofBirth,
        'UserProfileUrl': userProfileUrl,
        'Address': address,
        'Latitude': latitude,
        'Longitude': longitude,
        'WorkHistory': workHistory,
        'UserTaskCategoryId': userTaskCategoryId,
        'Status': status,
        'MiddleName': middleName,
        'NamePrefix': namePrefix,
        'AreYouASmoker': areYouASmoker,
        'CountryCode': countryCode,
        'AddressLine1': addressLine1,
        'AddressLine2': addressLine2,
        'AddressLine3': addressLine3,
        'Town': town,
        'County': county,
        'Postcode': postcode,
        'TelNumber': telNumber,
        'NationalInsuranceNumber': nationalInsuranceNumber,
        'Nationality': nationality,
        'CountryofBirth': countryofBirth,
        'CountryofResidency': countryofResidency,
        'PassportNumber': passportNumber,
        'MaritalStatus': maritalStatus,
        'OccupantType': occupantType,
        'LivingDate': livingDate,
        'Password': password,
        'UserCompanyId': userCompanyId,
        'VisaExpiryDate': visaExpiryDate,
        'PassportExpiryDate': passportExpiryDate,
        'VisaName': visaName,
        'OtherVisaName': otherVisaName,
        'Id': id,
      };
}
