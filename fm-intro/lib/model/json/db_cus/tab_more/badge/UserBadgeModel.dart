class UserBadgeModel {
  int userId;
  dynamic user;
  int status;
  String creationDate;
  String title;
  String type;
  String description;
  String referenceId;
  String referenceType;
  String refrenceNumber;
  String refrenceUrl;
  String refrence;
  String accountNumber;
  String accountName;
  String swiftCode;
  bool isVerified;
  String verificationCode;
  String profileImageUrl;
  String profileOwnerName;
  String iPAddress;
  int id;

  UserBadgeModel(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.title,
      this.type,
      this.description,
      this.referenceId,
      this.referenceType,
      this.refrenceNumber,
      this.refrenceUrl,
      this.refrence,
      this.accountNumber,
      this.accountName,
      this.swiftCode,
      this.isVerified,
      this.verificationCode,
      this.profileImageUrl,
      this.profileOwnerName,
      this.iPAddress,
      this.id});

  UserBadgeModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'] ?? {};
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    title = json['Title'] ?? '';
    type = json['Type'] ?? '';
    description = json['Description'] ?? '';
    referenceId = json['ReferenceId'] ?? '';
    referenceType = json['ReferenceType'] ?? '';
    refrenceNumber = json['RefrenceNumber'] ?? '';
    refrenceUrl = json['RefrenceUrl'] ?? '';
    refrence = json['Refrence'] ?? '';
    accountNumber = json['AccountNumber'] ?? '';
    accountName = json['AccountName'] ?? '';
    swiftCode = json['SwiftCode'] ?? '';
    isVerified = json['IsVerified'] ?? false;
    verificationCode = json['VerificationCode'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    profileOwnerName = json['ProfileOwnerName'] ?? '';
    iPAddress = json['IPAddress'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['Title'] = this.title;
    data['Type'] = this.type;
    data['Description'] = this.description;
    data['ReferenceId'] = this.referenceId;
    data['ReferenceType'] = this.referenceType;
    data['RefrenceNumber'] = this.refrenceNumber;
    data['RefrenceUrl'] = this.refrenceUrl;
    data['Refrence'] = this.refrence;
    data['AccountNumber'] = this.accountNumber;
    data['AccountName'] = this.accountName;
    data['SwiftCode'] = this.swiftCode;
    data['IsVerified'] = this.isVerified;
    data['VerificationCode'] = this.verificationCode;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['ProfileOwnerName'] = this.profileOwnerName;
    data['IPAddress'] = this.iPAddress;
    data['Id'] = this.id;
    return data;
  }
}
