class NotiModel {
  var isRead;
  var publishDateTime;
  var readDateTime;
  var entityId;
  var entityName;
  var entityTitle;
  var entityDescription;
  var eventName;
  var initiatorId;
  var initiatorDisplayName;
  var initiatorImageUrl;
  var message;
  var webUrl;
  var description;
  var receivedName;
  var userId;
  var notificationEventId;
  var id;

  NotiModel({
    this.isRead,
    this.publishDateTime,
    this.readDateTime,
    this.entityId,
    this.entityName,
    this.entityTitle,
    this.entityDescription,
    this.eventName,
    this.initiatorId,
    this.initiatorDisplayName,
    this.initiatorImageUrl,
    this.message,
    this.webUrl,
    this.description,
    this.receivedName,
    this.userId,
    this.notificationEventId,
    this.id,
  });

  factory NotiModel.fromJson(Map<String, dynamic> j) {
    return NotiModel(
      isRead: j['IsRead'] ?? false,
      publishDateTime: j['PublishDateTime'] ?? '',
      readDateTime: j['ReadDateTime'] ?? '',
      entityId: j['EntityId'] ?? 0,
      entityName: j['EntityName'] ?? '',
      entityTitle: j['EntityTitle'] ?? '',
      entityDescription: j['EntityDescription'] ?? '',
      eventName: j['EventName'] ?? '',
      initiatorId: j['InitiatorId'] ?? 0,
      initiatorDisplayName: j['InitiatorDisplayName'] ?? '',
      initiatorImageUrl: j['InitiatorImageUrl'] ?? '',
      message: j['Message'] ?? '',
      webUrl: j['WebUrl'] ?? '',
      description: j['Description'] ?? '',
      receivedName: j['ReceivedName'] ?? '',
      userId: j['UserId'] ?? 0,
      notificationEventId: j['NotificationEventId'] ?? 0,
      id: j['Id'] ?? 0,
    );
  }

  Map<String, dynamic> toMap() => {
        'IsRead': isRead,
        'PublishDateTime': publishDateTime,
        'ReadDateTime': readDateTime,
        'EntityId': entityId,
        'EntityName': entityName,
        'EntityTitle': entityTitle,
        'EntityDescription': entityDescription,
        'EventName': eventName,
        'InitiatorId': initiatorId,
        'InitiatorDisplayName': initiatorDisplayName,
        'InitiatorImageUrl': initiatorImageUrl,
        'Message': message,
        'WebUrl': webUrl,
        'Description': description,
        'ReceivedName': receivedName,
        'UserId': userId,
        'NotificationEventId': notificationEventId,
        'Id': id,
      };
}
