class PostKbaCrditQAAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostKbaCrditQAAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostKbaCrditQAAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  AnsKbaQuestions ansKbaQuestions;

  ResponseData({this.ansKbaQuestions});

  ResponseData.fromJson(Map<String, dynamic> json) {
    ansKbaQuestions = json['AnsKbaQuestions'] != null
        ? new AnsKbaQuestions.fromJson(json['AnsKbaQuestions'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.ansKbaQuestions != null) {
      data['AnsKbaQuestions'] = this.ansKbaQuestions.toJson();
    }
    return data;
  }
}

class AnsKbaQuestions {
  AnswerKbaQuestionsResult answerKbaQuestionsResult;
  AnsKbaQuestions({this.answerKbaQuestionsResult});
  AnsKbaQuestions.fromJson(Map<String, dynamic> json) {
    answerKbaQuestionsResult = json['AnswerKbaQuestionsResult'] != null
        ? new AnswerKbaQuestionsResult.fromJson(
            json['AnswerKbaQuestionsResult'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.answerKbaQuestionsResult != null) {
      data['AnswerKbaQuestionsResult'] = this.answerKbaQuestionsResult.toJson();
    }
    return data;
  }
}

class AnswerKbaQuestionsResult {
  String status;
  String outcome;

  AnswerKbaQuestionsResult({this.status, this.outcome});

  AnswerKbaQuestionsResult.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    outcome = json['Outcome'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['Outcome'] = this.outcome;
    return data;
  }
}
