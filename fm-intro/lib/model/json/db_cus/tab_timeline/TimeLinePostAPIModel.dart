import 'TimeLinePostModel.dart';

class TimeLinePostAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  _ErrorMessages messages;
  _ResponseData responseData;

  TimeLinePostAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  TimeLinePostAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new _ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new _ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new _ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class _ErrorMessages {
  _ErrorMessages();
  _ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class _ResponseData {
  bool success;
  TimeLinePostModel post;

  _ResponseData({this.success, this.post});

  _ResponseData.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    post = json['Post'] != null
        ? new TimeLinePostModel.fromJson(json['Post'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.post != null) {
      data['Post'] = this.post.toJson();
    }
    return data;
  }
}
