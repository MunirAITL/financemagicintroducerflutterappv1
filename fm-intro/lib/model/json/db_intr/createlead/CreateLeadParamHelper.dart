class CreateLeadParamHelper {
  int id;
  int status;
  String title;
  String description;
  String remarks;
  int initiatorId;
  String serviceDate;
  String resolutionType;
  int parentId;
  int assigneeId;
  String assigneeName;
  int userCompanyId;
  String leadStatus;
  String firstName;
  String middleName;
  String lastName;
  String mobileNumber;
  String email;
  String dateofBirth;
  int referenceId;
  String stage;
  int probability;
  String titleOthers;
  int estimatedEarning;
  int isLockAutoCall;
  int supportAdminId;
  int userNoteEntityId;
  String userNoteEntityName;
  int negotiatorId;
  String phoneNumber;
  String emailAddress;
  String leadNote;

  CreateLeadParamHelper(
      {this.id,
        this.status,
        this.title,
        this.description,
        this.remarks,
        this.initiatorId,
        this.serviceDate,
        this.resolutionType,
        this.parentId,
        this.assigneeId,
        this.assigneeName,
        this.userCompanyId,
        this.leadStatus,
        this.firstName,
        this.middleName,
        this.lastName,
        this.mobileNumber,
        this.email,
        this.dateofBirth,
        this.referenceId,
        this.stage,
        this.probability,
        this.titleOthers,
        this.estimatedEarning,
        this.isLockAutoCall,
        this.supportAdminId,
        this.userNoteEntityId,
        this.userNoteEntityName,
        this.negotiatorId,
        this.phoneNumber,
        this.emailAddress,
        this.leadNote});

  CreateLeadParamHelper.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    status = json['Status'];
    title = json['Title'];
    description = json['Description'];
    remarks = json['Remarks'];
    initiatorId = json['InitiatorId'];
    serviceDate = json['ServiceDate'];
    resolutionType = json['ResolutionType'];
    parentId = json['ParentId'];
    assigneeId = json['AssigneeId'];
    assigneeName = json['AssigneeName'];
    userCompanyId = json['UserCompanyId'];
    leadStatus = json['LeadStatus'];
    firstName = json['FirstName'];
    middleName = json['MiddleName'];
    lastName = json['LastName'];
    mobileNumber = json['MobileNumber'];
    email = json['Email'];
    dateofBirth = json['DateofBirth'];
    referenceId = json['ReferenceId'];
    stage = json['Stage'];
    probability = json['Probability'];
    titleOthers = json['TitleOthers'];
    estimatedEarning = json['EstimatedEarning'];
    isLockAutoCall = json['IsLockAutoCall'];
    supportAdminId = json['SupportAdminId'];
    userNoteEntityId = json['UserNoteEntityId'];
    userNoteEntityName = json['UserNoteEntityName'];
    negotiatorId = json['NegotiatorId'];
    phoneNumber = json['PhoneNumber'];
    emailAddress = json['EmailAddress'];
    leadNote = json['LeadNote'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Status'] = this.status;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['Remarks'] = this.remarks;
    data['InitiatorId'] = this.initiatorId;
    data['ServiceDate'] = this.serviceDate;
    data['ResolutionType'] = this.resolutionType;
    data['ParentId'] = this.parentId;
    data['AssigneeId'] = this.assigneeId;
    data['AssigneeName'] = this.assigneeName;
    data['UserCompanyId'] = this.userCompanyId;
    data['LeadStatus'] = this.leadStatus;
    data['FirstName'] = this.firstName;
    data['MiddleName'] = this.middleName;
    data['LastName'] = this.lastName;
    data['MobileNumber'] = this.mobileNumber;
    data['Email'] = this.email;
    data['DateofBirth'] = this.dateofBirth;
    data['ReferenceId'] = this.referenceId;
    data['Stage'] = this.stage;
    data['Probability'] = this.probability;
    data['TitleOthers'] = this.titleOthers;
    data['EstimatedEarning'] = this.estimatedEarning;
    data['IsLockAutoCall'] = this.isLockAutoCall;
    data['SupportAdminId'] = this.supportAdminId;
    data['UserNoteEntityId'] = this.userNoteEntityId;
    data['UserNoteEntityName'] = this.userNoteEntityName;
    data['NegotiatorId'] = this.negotiatorId;
    data['PhoneNumber'] = this.phoneNumber;
    data['EmailAddress'] = this.emailAddress;
    data['LeadNote'] = this.leadNote;
    return data;
  }
}
