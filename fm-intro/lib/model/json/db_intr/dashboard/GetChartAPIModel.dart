class GetChartAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetChartAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetChartAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<CaseCountMonthlyWiseSummaryReports> caseCountMonthlyWiseSummaryReports;
  ResponseData({this.caseCountMonthlyWiseSummaryReports});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['CaseCountMonthlyWiseSummaryReports'] != null) {
      caseCountMonthlyWiseSummaryReports =
          <CaseCountMonthlyWiseSummaryReports>[];
      json['CaseCountMonthlyWiseSummaryReports'].forEach((v) {
        caseCountMonthlyWiseSummaryReports
            .add(new CaseCountMonthlyWiseSummaryReports.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.caseCountMonthlyWiseSummaryReports != null) {
      data['CaseCountMonthlyWiseSummaryReports'] = this
          .caseCountMonthlyWiseSummaryReports
          .map((v) => v.toJson())
          .toList();
    }
    return data;
  }
}

class CaseCountMonthlyWiseSummaryReports {
  String dateOfMonth;
  int totalCase;
  int totalCompletedCase;

  CaseCountMonthlyWiseSummaryReports(
      {this.dateOfMonth, this.totalCase, this.totalCompletedCase});

  CaseCountMonthlyWiseSummaryReports.fromJson(Map<String, dynamic> json) {
    dateOfMonth = json['DateOfMonth'];
    totalCase = json['TotalCase'];
    totalCompletedCase = json['TotalCompletedCase'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DateOfMonth'] = this.dateOfMonth;
    data['TotalCase'] = this.totalCase;
    data['TotalCompletedCase'] = this.totalCompletedCase;
    return data;
  }
}
