class UserPaymentSummaryDataModel {
  int userId;
  double totalIncomeAmount;
  double totalPaidAmount;
  double totalPipelineAmount;
  double totalAdviserAmount;
  double totalAdviserPaidAmount;
  double totalAdviserPipelineAmount;
  int id;

  UserPaymentSummaryDataModel(
      {this.userId,
      this.totalIncomeAmount,
      this.totalPaidAmount,
      this.totalPipelineAmount,
      this.totalAdviserAmount,
      this.totalAdviserPaidAmount,
      this.totalAdviserPipelineAmount,
      this.id});

  UserPaymentSummaryDataModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    totalIncomeAmount = json['TotalIncomeAmount'] ?? 0.0;
    totalPaidAmount = json['TotalPaidAmount'] ?? 0.0;
    totalPipelineAmount = json['TotalPipelineAmount'] ?? 0.0;
    totalAdviserAmount = json['TotalAdviserAmount'] ?? 0.0;
    totalAdviserPaidAmount = json['TotalAdviserPaidAmount'] ?? 0.0;
    totalAdviserPipelineAmount = json['TotalAdviserPipelineAmount'] ?? 0.0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['TotalIncomeAmount'] = this.totalIncomeAmount;
    data['TotalPaidAmount'] = this.totalPaidAmount;
    data['TotalPipelineAmount'] = this.totalPipelineAmount;
    data['TotalAdviserAmount'] = this.totalAdviserAmount;
    data['TotalAdviserPaidAmount'] = this.totalAdviserPaidAmount;
    data['TotalAdviserPipelineAmount'] = this.totalAdviserPipelineAmount;
    data['Id'] = this.id;
    return data;
  }
}
