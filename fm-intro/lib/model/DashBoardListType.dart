class DashBoardListType{
  String type;
  String taskID;
  String title;
  String initiatorName;
  String comments;

  DashBoardListType({this.type, this.taskID, this.title, this.initiatorName, this.comments});
}