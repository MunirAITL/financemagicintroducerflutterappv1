//Singleton reusable class
//  https://medium.com/@nshansundar/simple-observer-pattern-to-notify-changes-across-screens-in-flutter-dart-f035dbd990a9

abstract class StateListener {
  void onStateChanged(ObserverState state);
  void onStateChangedWithData(ObserverState state, data) {}
}

enum ObserverState {
  STATE_CHANGED_tabbar1_reload_case_api,
  STATE_CHANGED_tabbar2_reload_case_api,
  STATE_CHANGED_tabbar3_reload_case_api,
  STATE_CHANGED_tabbar4_reload_case_api,
  STATE_CHANGED_tabbar1,
  STATE_CHANGED_tabbar2,
  STATE_CHANGED_tabbar3,
  STATE_CHANGED_tabbar4,
  STATE_CHANGED_tabbar5,
  STATE_CHANGED_tabbar6,
  STATE_CHANGED_logout,
  STATE_CHANGED_full_report_credit_report,
  STATE_WEBVIEW_BACK,
  STATE_HREF_LOGIN,
}

class StateProvider {
  List<StateListener> observers = [];
  static final StateProvider _instance = new StateProvider.internal();
  factory StateProvider() => _instance;
  StateProvider.internal() {
    //observers = new List<StateListener>();
    initState();
  }
  void initState() async {}

  void subscribe(StateListener listener) {
    observers.add(listener);
  }

  void unsubscribe(StateListener listener) {
    observers.remove(listener);
  }

  void notify(dynamic state) {
    observers.forEach((StateListener obj) => obj.onStateChanged(state));
  }

  void notifyWithData(dynamic state, dynamic data) {
    observers.forEach(
        (StateListener obj) => obj.onStateChangedWithData(state, data));
  }

  void dispose(StateListener thisObserver) {
    for (var obj in observers) {
      if (obj == thisObserver) {
        observers.remove(obj);
      }
    }
  }
}
