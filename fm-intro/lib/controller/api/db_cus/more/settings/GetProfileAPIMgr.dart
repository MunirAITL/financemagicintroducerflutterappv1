import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class GetProfileAPIMgr with Mixin {
  static final GetProfileAPIMgr _shared = GetProfileAPIMgr._internal();

  factory GetProfileAPIMgr() {
    return _shared;
  }

  GetProfileAPIMgr._internal();

  wsGetProfileAPI({
    BuildContext context,
    Function(UserProfileAPIModel) callback,
  }) async {
    var url = Server.Get_PROFILE_INFO_URL;
    url = url.replaceAll("#UserID#", userData.userModel.id.toString());
    await NetworkMgr()
        .req<UserProfileAPIModel, Null>(
      context: context,
      url: url,
      reqType: ReqType.Get,
    )
        .then((model) async {
      callback(model);
    });
  }

  wsGetLeadProfileAPI({
    BuildContext context,
    int id,
    Function(UserProfileAPIModel) callback,
  }) async {
    var url = Server.Get_PROFILE_INFO_URL;
    url = url.replaceAll("#UserID#", id.toString());
    await NetworkMgr()
        .req<UserProfileAPIModel, Null>(
      context: context,
      url: url,
      reqType: ReqType.Get,
    )
        .then((model) async {
      callback(model);
    });
  }
}
