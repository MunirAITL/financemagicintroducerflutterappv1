import 'package:aitl/controller/helper/db_intr/case_lead/CaseLeadDashboardHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_intr/caselead/CasePayInfoAPIModel.dart';
import 'package:aitl/model/json/db_intr/caselead/GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class DBIntrDashboardAPIMgr with Mixin {
  static final DBIntrDashboardAPIMgr _shared =
      DBIntrDashboardAPIMgr._internal();

  factory DBIntrDashboardAPIMgr() {
    return _shared;
  }

  DBIntrDashboardAPIMgr._internal();

  wsGetCasePayInfo({
    BuildContext context,
    Function(CasePayInfoAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<CasePayInfoAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: CaseLeadDashboardHelper().getURLCasePayInfo(),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsGetURLGetlLeadStatusIntrwiseReportDatabyIntr({
    BuildContext context,
    Function(GetlLeadStatusIntrwiseReportDatabyIntrAPIModel) callback,
  }) async {
    try {
      final url = CaseLeadDashboardHelper()
          .getURLGetlLeadStatusIntrwiseReportDatabyIntr();
      await NetworkMgr()
          .req<GetlLeadStatusIntrwiseReportDatabyIntrAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
