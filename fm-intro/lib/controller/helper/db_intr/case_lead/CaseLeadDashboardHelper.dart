import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:jiffy/jiffy.dart';

class CaseLeadDashboardHelper {
  getURLGetlLeadStatusIntrwiseReportDatabyIntr() {
    var url = ServerIntr.CASELEAD_DB_INT2_GET_URL;
    url = url.replaceAll(
        "#companyId#", userData.userModel.userCompanyID.toString());
    url = url.replaceAll("#criteria#", "1");
    url = url.replaceAll("#status#", "901");
    url = url.replaceAll("#isSpecificDate#", "All");
    url = url.replaceAll("#fromDateTime#", Jiffy().format("dd-MMM-yyyy"));
    url = url.replaceAll("#toDateTime#", Jiffy().format("dd-MMM-yyyy"));
    url = url.replaceAll("#advisorId#", "0");
    url = url.replaceAll("#introducerId#", userData.userModel.id.toString());
    url = url.replaceAll("#title#", "All");
    return url;
  }

  getURLCasePayInfo() {
    return ServerIntr.CASELEAD_DB_INTR1_GET_URL
        .replaceAll("#userId#", userData.userModel.id.toString());
  }
}
