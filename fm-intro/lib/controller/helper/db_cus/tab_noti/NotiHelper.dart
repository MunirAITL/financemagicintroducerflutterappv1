import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiModel.dart';
import 'package:aitl/config/db_cus/NotiCfg.dart';
import 'package:aitl/utils/Notifications/NotificationData.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';

class NotiHelper with Mixin {
  getUrl({pageStart, pageCount, caseStatus}) {
    var url = Server.NOTI_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    return url;
  }

  Map<String, dynamic> getNotiMap({NotiModel model}) {
    Map<String, dynamic> notiMap = {};
    try {
      // print("Get Noti Map = "+model.toMap().toString());
      // print(NotiCfg.EVENT_LIST.toString());
      for (var map in NotiCfg.EVENT_LIST) {
        final int listCommunityId = map["communityId"];
        int notificationEventId = map['notificationEventId'];
        //if (listCommunityId == int.parse(userData.userModel.communityID)) {
        if (model.entityName == map["entityName"] &&
            (model.notificationEventId == notificationEventId ||
                notificationEventId == 0)) {
          String txt = map["txt"];
          txt = txt.replaceAll(
              "#InitiatorDisplayName#", model.initiatorDisplayName);
          txt = txt.replaceAll("#CaseNo#", model.entityId.toString());
          txt = txt.replaceAll("#EventName#", model.eventName);
          txt = txt.replaceAll('[] ', '');
// print("case No 1 =  "+txt);
          //  text processing
          notiMap['txt'] = txt ?? '';
          // print("case No 2 =  "+notiMap['txt']);

          notiMap['publishDateTime'] =
              DateFun.getTimeAgoTxt(model.publishDateTime);

          //  event processing
          //notiMap['communityId'] = map['communityId'];
          //notiMap['desc'] = map['desc'];
          //notiMap['notificationEventId'] = map['notificationEventId'];
          notiMap['route'] =
              (map['route2'] == null) ? map['route'] : map['route2'];

          break;
        }
        //}
      }
    } catch (e) {
      print(e.toString());
    }
    return notiMap;
  }

  setRoute({
    BuildContext context,
    NotiModel notiModel,
    Map<String, dynamic> notiMap,
    Function callback,
  }) async {
    try {
      print("Taks bidding id = ${notiModel.entityId}");

      Type route = notiMap['route'];
      //if (identical(route, WebScreen)) {
      Get.to(
              () => WebScreen(
                    title: notiModel.message,
                    url: Server.BASE_URL_NOTI_WEB +
                        notiModel.webUrl
                            .toString()
                            .replaceAll("/application/about-me", ""),
                    caseID: notiModel.entityId.toString(),
                  ),
              transition: Transition.rightToLeft,
              duration:
                  Duration(milliseconds: AppConfig.pageAnimationMilliSecond))
          .then((value) {
        //callback(route);
      });
      //} else if (identical(route, TaskBiddingScreen)) {

      /*navTo(
          context: context,
          page: () => TaskBiddingScreen(
            title: notiModel.eventName,
            taskId: notiModel.entityId,
          ),
        ).then((value) {
          //callback(route);
        });
      } else {
        //if (identical(route, MsgTab)) {
        callback();
        //Navigator.pop(context);
      }*/
    } catch (e) {}
  }

  Map<String, dynamic> getNotiMapLocalNotification({NotificationData model}) {
    Map<String, dynamic> notiMap = {};
    try {
      for (var map in NotiCfg.EVENT_LIST) {
        int notificationEventId = map['notificationEventId'];

        if (model.entityName == map["entityName"] &&
            (model.notificationEventId.toString() ==
                    notificationEventId.toString() ||
                notificationEventId == 0)) {
          String txt = map["txt"];
          txt = txt.replaceAll("#InitiatorDisplayName#", model.initiatorName);
          txt = txt.replaceAll("#CaseNo#", model.entityId.toString());
          txt = txt.replaceAll("#EventName#", model.message);
          txt = txt.replaceAll('[] ', '');
          notiMap['txt'] = txt ?? '';

          break;
        }
      }
    } catch (e) {
      print(e.toString());
    }
    return notiMap;
  }
}

/*extension IndexedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}*/
