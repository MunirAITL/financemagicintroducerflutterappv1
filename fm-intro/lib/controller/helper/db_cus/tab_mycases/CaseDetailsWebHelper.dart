import 'package:aitl/config/Server.dart';

class CaseDetailsWebHelper {
  getLink({String title, int taskId}) {
    return getWebUrl(title: title, taskId: taskId);
  }

  getWebUrl({title, taskId}) {
    var url = Server.CASEDETAILS_WEBVIEW_URL;
    if (taskId > 0) {
      url = url.replaceAll("#title#", title.toString().replaceAll(' / ', ' '));
      url = url.replaceAll("#taskId#", taskId.toString());
    } else {
      url = url.replaceAll("#title#-#taskId#", "");
    }
    return url;
  }
}
