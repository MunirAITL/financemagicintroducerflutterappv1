class RegHelper {
  getParam({
    String email,
    String pwd,
    String fname = '',
    String lname = '',
    String fullName = '',
    String phone,
    String countryCode,
    String dob = '',
    String dobDD = '',
    String dobMM = '',
    String dobYY = '',
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "FirstName": fname,
      "LastName": lname,
      "MobileNumber": phone,
      "CommunityId": "19",
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": countryCode,
      "Status": "101",
      "OTPCode": "",
      "BirthDay": dobDD,
      "BirthMonth": dobMM,
      "BirthYear": dobYY,
      // "UserCompanyId": 1003,
      // "UserCompanyId": 1367,
      "UserCompanyId": 2,
      "dialCode": countryCode,
      "ConfirmPassword": pwd,
      "confirmPassword": pwd,
      "DateofBirth": dob,
    };
  }
}
