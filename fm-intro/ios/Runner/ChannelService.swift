//
//  ChannelService.swift
//  Runner
//
//  Created by DEV on 6/1/22.
//

import Foundation

import UIKit
import Flutter
import Flutter

enum ChannelName {
  static let battery = "mm4.flutter.io/mm4"
  static let charging = "mm4.flutter.io/mm4"
}

enum MM4State :String {
    case avialable = "yes"
    case na = "no"
}

enum MyFlutterErrorCode {
  static let unavailable = "UNAVAILABLE"
}


class ChannelService : FlutterAppDelegate {    
    
    private var eventChannel: FlutterEventChannel?
      
    private let linkStreamHandler = LinkStreamHandler()
    
    func startService(view:AnyObject) {
        
        if let flutterViewController  = window?.rootViewController {
                
            let flutterChannel = FlutterMethodChannel.init(name: "hrefLogin", binaryMessenger: flutterViewController as! FlutterBinaryMessenger);
                flutterChannel.setMethodCallHandler { (flutterMethodCall, flutterResult) in
                    if flutterMethodCall.method == "loginEmailActivity" {
                                              
                        
                        UIView.animate(withDuration: 0.5, animations: {
                            self.window?.rootViewController = nil
                            
                            let rootViewController : FlutterViewController = self.window?.rootViewController as! FlutterViewController
                            let channel = FlutterMethodChannel(name: "flutter.native/mm4", binaryMessenger: rootViewController as! FlutterBinaryMessenger)
                            // Invoke a Dart method.
                            let email = "email" // or "baz", or "unknown"
                            let token = "token"
                            var param : [String: String] = [:]
                            param["email"] = email;
                            param["token"] = token;
                            channel.invokeMethod("invokeMethod", arguments: param) {
                              (result: Any?) -> Void in
                              if let error = result as? FlutterError {
                                  print(error.description)
                              } else if FlutterMethodNotImplemented.isEqual(result) {
                               
                              } else {
                                print("service not available")
                              }
                            }
                            // Receive method invocations from Dart and return results.
                            channel.setMethodCallHandler {
                              (call: FlutterMethodCall, result: FlutterResult) -> Void in
                              switch (call.method) {
                              case "email": result("test href login data, \(call.arguments as! String)")
                              case "token": result(FlutterError(
                                code: "400", message: "asdfasf67676676asdfwer", details: nil))
                              default: result(FlutterMethodNotImplemented)
                            }
                            
                            let navigationController = UINavigationController(rootViewController: flutterViewController)
                            
                            self.window = UIWindow(frame: UIScreen.main.bounds)
                            self.window?.makeKeyAndVisible()
                            self.window.rootViewController = navigationController
                            navigationController.isNavigationBarHidden = false
                            navigationController.pushViewController(view as! UIViewController, animated: false)
                            
                        }
                    });
                }
            }
        }
    }
    
    func redirect(appScheme: AnyObject) -> AnyObject {
        eventChannel?.setStreamHandler(linkStreamHandler)
        return linkStreamHandler.handleLink(appScheme.absoluteString!) as AnyObject
    }
}

class LinkStreamHandler:NSObject, FlutterStreamHandler {
  
  var eventSink: FlutterEventSink?
  
  // links will be added to this queue until the sink is ready to process them
  var queuedLinks = [String]()
  
  func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
    self.eventSink = events
    queuedLinks.forEach({ events($0) })
    queuedLinks.removeAll()
    return nil
  }
  
  func onCancel(withArguments arguments: Any?) -> FlutterError? {
    self.eventSink = nil
    return nil
  }
  
  func handleLink(_ link: String) -> Bool {
    guard let eventSink = eventSink else {
      queuedLinks.append(link)
      return false
    }
    eventSink(link)
    return true
  }
}
